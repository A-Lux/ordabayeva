$(document).ready(function () {

    const searchWrapper = document.querySelector(".search-input");
    const inputBox = searchWrapper.querySelector("input");
    const suggBox = searchWrapper.querySelector(".autocom-box");
    let suggestions = [
    "Channel",
    "CodingLab",
    "CodingNepal",
    "YouTube",
    "YouTuber",
    "YouTube Channel",
    "Blogger",
    "Bollywood",
    "Vlogger",
    "Vechiles",
    "Facebook",
    "Freelancer",
    "Facebook Page",
    "Designer",
    "Developer",
    "Web Designer",
    "Web Developer",
    "Login Form in HTML & CSS",
    "How to learn HTML & CSS",
    "How to learn JavaScript",
    "How to become Freelancer",
    "How to become Web Designer",
    "How to start Gaming Channel",
    "How to start YouTube Channel",
    "What does HTML stands for?",
    "What does CSS stands for?",
];

    inputBox.onkeyup = (e) => {
        let userData = e.target.value; //user enetered data
        let emptyArray = [];
        if (userData) {
            emptyArray = suggestions.filter((data) => {
                //filtering array value and user characters to lowercase and return only those words which are start with user enetered chars
                return data.toLocaleLowerCase().startsWith(userData.toLocaleLowerCase());
            });
            emptyArray = emptyArray.map((data) => {
                // passing return data inside li tag
                return data = `<li>${data}</li>`;
            });
            searchWrapper.classList.add("active"); //show autocomplete box
            showSuggestions(emptyArray);
            let allList = suggBox.querySelectorAll("li");
            for (let i = 0; i < allList.length; i++) {
                //adding onclick attribute in all li tag
                allList[i].setAttribute("class", "search-li");
            }
        } else {
            searchWrapper.classList.remove("active"); //hide autocomplete box
        }

        //        $('.search-li').on('click', function() {
        //            searchWrapper.classList.remove("active");
        //            inputBox.value() = $('.search-li').html;
        //        });
    }

    function showSuggestions(list) {
        let listData;
        if (!list.length) {
            userValue = inputBox.value;
            listData = `<li>${userValue}</li>`;
        } else {
            listData = list.join('');
        }
        suggBox.innerHTML = listData;
    }

});

$(function () {

    // ------ Кнопка Каталог --------
    let openAdd = false

    $(".header__catalog").click(function () {
        $('.burger').toggleClass('burger--active');
        if (openAdd) {
            $('.h-catalog').fadeOut('fast');
        } else {
            $('.h-catalog').fadeIn('fast');
        }
        openAdd = openAdd ? false : true
    });

    let filterOpen = false

    // $(".filter__box-title").click(function () {
    //     if (filterOpen) {
    //         $('.filter__none').fadeOut('fast');
    //     }
    //     else {
    //         $('.filter__none').fadeIn('fast');
    //     }
    //     filterOpen = filterOpen ? false : true
    // });

    $('.open-1').click(function () {
        $('.none-1').toggle();
        $('.open-1 img').toggleClass('active');
    });

    $('.open-2').click(function () {
        $('.none-2').toggle();
        $('.open-2 img').toggleClass('active');
    });

    $('.open-3').click(function () {
        $('.none-3').toggle();
        $('.open-3 img').toggleClass('active');
    });

    $('.open-4').click(function () {
        $('.none-4').toggle();
        $('.open-4 img').toggleClass('active');
    });

    $('.filter__box1-title').click(function () {
        $(this).toggleClass('active')
        $('.filter__box1-item').toggle();
    });

    $('.filter__box-title').click(function () {
        $(this).toggleClass('active')
    });
    // ----------------Modal Вход ---------------------
    $('[data-modal=login]').on('click', function (e) {
        e.preventDefault();
        $('.overlay2, #login').fadeIn('slow');
        $('.overlay3, #registration').fadeOut('slow');
    });
    $('.modal__close').on('click', function () {
        $('.overlay2, #login').fadeOut('slow')
    });

    // --------------Modal Регистрация -------------------
    $('[data-modal=registration]').on('click', function (e) {
        e.preventDefault();
        $('.overlay2, #login').fadeOut('slow');
        $('.overlay3, #registration').fadeIn('slow');
    });
    $('.modal__close').on('click', function () {
        $('.overlay3, #registration').fadeOut('slow')
    });

    // ------ sliders -------

    $('.main__sliders').slick({
        arrows: false,
        infinite: true,
        speed: 300,
        autoplay: true,
        dots: true
    });

    $('.interest__sliders').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow: '<button class="slider-arrow slider-left"> <img src="../img/main-page/left.svg"> </button>',
        nextArrow: '<button class="slider-arrow slider-right"> <img src="../img/main-page/right.svg"> </button>',
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    prevArrow: '<button class="slider-arrow slider-left"> <img src="../img/main-page/left.svg"> </button>',
                    nextArrow: '<button class="slider-arrow slider-right"> <img src="../img/main-page/right.svg"> </button>',
                },
            },
            {
                breakpoint: 991,
                settings: {
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    prevArrow: '<button class="slider-arrow slider-left"> <img src="../img/main-page/left.svg"> </button>',
                    nextArrow: '<button class="slider-arrow slider-right"> <img src="../img/main-page/right.svg"> </button>',
                },
            },
            {
                breakpoint: 767,
                settings: {
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    prevArrow: '<button class="slider-arrow slider-left"> <img src="../img/main-page/left.svg"> </button>',
                    nextArrow: '<button class="slider-arrow slider-right"> <img src="../img/main-page/right.svg"> </button>',
                }
            }
        ]
    });

    //------- табы гл Страницы ---------

    $('ul.catalog__tabs').on('click', 'li:not(.catalog__tab-active)', function () {
        $(this)
            .addClass('catalog__tab-active').siblings().removeClass('catalog__tab-active')
            .closest('div.catalog__inner').find('div.catalog__items').removeClass('catalog__items-active').eq($(this).index()).addClass('catalog__items-active');
    });

    //-------- Счётчик ----------

    let curAmount = 1
    const amount = $('.amount')
    const minusButton = $('.minusButton')
    const plusButton = $('.plusButton')
    minusButton.on('click', () => {
        if (curAmount > 1) amount.text(--curAmount + '')
    })
    plusButton.on('click', () => {
        amount.text(++curAmount + '')
    })

    //-------- Табы Account.html ----------

    let qur = 0
    if ($('.hidden_router')[0].value.includes('75')) {
        qur = 2
        $('.tabs:nth-child(3)').show()
    } else
        $(".tabs").first().show();
    $(".account__tab-link")[qur].classList.add('account__tab-link-active')
    $(".account__tab-link").click(function () {
        $(".account__tab-link")[qur].classList.remove('account__tab-link-active')
        $(this).addClass('account__tab-link-active')
        qur = $(".account__tab-link").index(this)
        $(".tabs").hide().eq($(this).index()).fadeIn()
    }).eq(0).addClass(".account__tab-link-active");

});
