@extends('layouts.header')
@section('content')
    <section class="link1">
        <div class="container-fluid">
            <div class="link1__inner">
                <div class="link1__img">
                    <img src="{{asset('img/link1.jpg')}}" alt=""/>
                </div>
                <div class="link1__wrap">
                    <div class="link1__title">О нашей компании</div>
                    <div class="link1__subtitle">
                        Основана в 2004 году, ребрендирована в QazMedPro в 2019 Головной
                        офис располагается в г. Алматы
                    </div>
                    <div class="link1__descr">
                        <p>
                            Наша Миссия - организация медицинских пунктов и оказание
                            догоспитальной квалифицированной медицинской помощи для защиты
                            здоровья и жизни вашего персонала.
                        </p>
                        <p>
                            Мы обслуживаем более 100 предприятий в Казахстане Наши медики
                            работаю в более 70 медицинских пунктов по всему Казахстану Наши
                            клиенты работают в нефтегазовом секторе, строительстве,
                            добывающей и перерабатывающей индустриях.
                        </p>
                        <p>
                            Наш штат состоит из 250 медицинских работников, распределенных
                            между медицинскими пунктами и офисами компании в Алматы, Атырау,
                            Шымкент, Степногорск, Караганда, Экибастуз. Мы имеем
                            сертификацию ISO (система менеджмента качества, система
                            экологического менеджмента, система менеджмента безопасности
                            труда и охраны здоровья) Успешно прошли предквалификационный
                            отбор в качестве потенциального поставщика услуг для предприятий
                            Самрук-Казына, сертификацию ALASH в соответствии с «Актауской
                            декларацией о совместных действиях», подписанной KPO, KMG, NCOC,
                            TCO от 25/09/12
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer class="footer">
        <div class="container-fluid">
            <div class="footer__inner">
                <button onclick="topFunction()" id="myBtn" class="footer__up">
                    <img src="{{asset('img/main-page/footer-arrow.svg')}}" alt=""/>
                </button>
                <div class="footer__links offset-md-2 col-md-8">
                    <a href="{{ route('delivery') }}" class="footer__link">Доставка и оплата</a>
                    {{--                    <a href="" class="footer__link">Условия возврата</a>--}}
                    <a href="{{ route('company') }}" class="footer__link">О компании</a>
                    <a href="{{ route('contacts') }}" class="footer__link">Контакты</a>
                </div>
                <div class="footer__wrap">
                    <a href="" class="footer__social"
                    ><img src="{{asset('img/main-page/vk.svg')}}" alt=""
                        /></a>
                    <a href="" class="footer__social">
                        <img src="{{asset('img/main-page/insta.svg')}}" alt=""/> </a
                    ><a href="" class="footer__social"
                    ><img src="{{asset('img/main-page/facebook.svg')}}" alt=""
                        /></a>
                </div>
                <div class="footer__title">
                </div>
            </div>
        </div>
    </footer>
    <script>
        var mybutton = document.getElementById("myBtn");
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
@endsection
