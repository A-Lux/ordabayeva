@extends('layouts.header')
@section('content')
    <input type="hidden" class="hidden_router" value="{{ Request::url() }}" style="display: none">
    <div class="container-fluid">
        <section class="account">
            <div class="account__inner">
                <div class="account__navbar">
                    <div class="account__photo">
                        <img src="img/account-page/personal-photo.png" alt=""/>
                    </div>
                    <div class="account__name">Купря Резонанс</div>
                    <div class="account__links">

                        <div class="account__link account__tab-link">Личные данные</div>
                        <div class="account__link account__tab-link">Мои заказы</div>
                        <div class="account__link account__tab-link">Избранное</div>
                        {{--                        <form action="{{ route('logout') }}" method="POST">--}}
                        {{--                            {{csrf_field()}}--}}
                        {{--                            <a href="{{ route('logout') }}">--}}
                        {{--                        <div class="account__link account__link-logout">Выход</div>--}}
                        {{--                            </a>--}}
                        {{--                        </form>--}}
                        <div class="account__link account__link-logout">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Выход
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>

                        {{--                        </form>--}}


                    </div>
                </div>
                <div class="account__wrap">
                    <!-- =============== Раздел - Личные данные ================ -->

                    <div class="tabs">
                        <div class="account__personal-date">
                            <div class="account__personal-date-title">
                                Контактные данные
                            </div>
                            <form action="{{ route('update') }}" class="account__personal-date-form" method="post">
                                @csrf
                                <div class="account__personal-date-items">
                                    <div class="account__personal-date-box">
                                        <div class="account__personal-date-titles">Имя</div>
                                        <input type="text" name="name" value="{{ auth()->check() ? auth()->user()->name : ''}}" class="account__personal-date-input"/>
                                    </div>
                                    <div class="account__personal-date-box">
                                        <div class="account__personal-date-titles">Фамилия</div>
                                        <input type="text" name="surname" value="{{ auth()->check() ? auth()->user()->surname : ''}}" class="account__personal-date-input"/>
                                    </div>
                                    <div class="account__personal-date-box">
                                        <div class="account__personal-date-titles">Телефон</div>
                                        <input type="text" name="phone" value="{{ auth()->check() ? auth()->user()->phone : ''}}" class="account__personal-date-input"/>
                                    </div>
                                    <div class="account__personal-date-box">
                                        <div class="account__personal-date-titles">E-mail</div>
                                        <input type="text" name="email" value="{{ auth()->check() ? auth()->user()->email : ''}}" class="account__personal-date-input"/>
                                    </div>
                                    <div class="account__personal-date-box">
                                        <div class="account__personal-date-titles">Пароль</div>
                                        <input
                                            type="password"
                                            class="account__personal-date-input"
                                            name="password"
                                        />
                                    </div>
                                </div>
                                <div class="account__personal-date-male">
                                    <span>Пол:</span>
                                    <div class="account__personal-date-check">
                                        <input
                                            type="checkbox"
                                            class="account__personal-date-maleInput"
                                            name="is_male"
                                            id="is_male"
                                            onclick="document.getElementById('is_female').checked=false"
{{--                                            {{$user ?? ''->is_male == 1 ? 'checked': ''}}--}}
                                            {{(auth()->check() ? auth()->user()->is_male : '' == 1) ? 'checked' : ''}}
{{--                                            {{$user->is_male == 1 ? 'checked' : ''}}--}}
                                        />
                                        Мужской
                                    </div>
                                    <div class="account__personal-date-check">
                                        <input
                                            type="checkbox"
                                            class="account__personal-date-maleInput"
                                            name="is_female"
                                            id="is_female"
                                            onclick="document.getElementById('is_male').checked=false"
{{--                                            {{$user ?? ''->is_male == 0 ? 'checked': ''}}--}}
{{--                                            {{$user->is_male == 1 ? '':'checked'}}--}}
                                            {{(auth()->check() ? auth()->user()->is_male : '' == 1) ? '' : 'checked'}}

                                        />
                                        Женский
                                    </div>
                                </div>
                                <button type="submit" class="button account__personal-date-btn">
                                    Сохранить изменения
                                </button>
                            </form>
                        </div>
                    </div>

                    <!-- =============== Раздел - Мои заказы =================== -->

                    <div class="tabs">
                        <div class="account__order">
                            <div class="account__order-items">
                                @if(isset($order))
                                    @foreach($order as $tito)


                                        <div class="account__order-item">
                                            <div class="account__order-wrap">
                                                <div class="account__order-number">{{$tito->order_number}}</div>

                                                <div class="account__order-delivery">
                                                    Доставлено <span>{{$tito->updated_at}} 00:06 {{$tito->id}}</span>
                                                </div>
                                                <div class="account__order-cost">{{$tito->total_price}} тг.</div>
                                            </div>
                                            <div class="account__order-wrap">
                                                <div class="account__order-productPhoto">
                                                    @foreach(json_decode($tito->order_details) as $j)
                                                        <img src="{{ 'storage/'.$j->image }}" alt=""/>
                                                        @break
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>

                    <!-- =============== Раздел - Избранные ==================== -->

                    <div class="tabs">
                        <div class="account__favorite">
                            <div class="account__favorite-inner">
                                <div class="catalog__items catalog__items-active">
                                    @if(isset($wishlist))
                                        @foreach($wishlist as $room)
{{--                                            @dd($wishlist)--}}
                                            <div class="catalog__item" style="display:flex; flex-direction: column;">
                                                <div class="catalog__item-wrap">
                                                    <div class="catalog__item-favorite">
                                                        <img
                                                            src="img/main-page/catalog-favorite.svg"
                                                            alt=""
                                                        />
                                                    </div>
                                                    {{--                          @if($room->is_new == 1)--}}
                                                    <div class="catalog__item-new">new</div>
                                                    {{--                              @endif--}}
                                                </div>
                                                @if(isset($room->product['image']))

                                                @foreach(json_decode($room->product->image) as $image)
                                                    <img
                                                        class="catalog__item-img" style="width: 100%"
                                                        src="{{ url('storage/'.$image) }}"
                                                        alt=""
                                                    />
                                                    @break
                                                @endforeach
                                                @endif
                                                <div class="catalog__item-title">
                                                    {{ $room->product->product_name}}
                                                </div>
                                                <div class="catalog__item-subtitle" style="flex: 1 1 auto">
                                                    {{ $room->product->description }}
                                                </div>
                                                <div class="catalog__item-wrap2">
                                                    <div
                                                        class="catalog__item-stock">{{$room->product->stock == 1 ? 'Есть в наличии': 'Нет в наличии' }}</div>
                                                    <div class="catalog__item-cost">{{ $room->product->price }} ₸</div>
                                                    <div class="catalog__item-basket">
                                                        <a href="{{ route('add_cart', $room->product->id) }}">
                                                            <img src="img/main-page/catalog-basket.svg" alt=""/>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="basket__item-click" style="text-align: center; margin-top: 5%">
                                                    <a href="{{ route('remove_wishlist', $room->id) }}">Удалить
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                    {{--                                    <div class="catalog__item">--}}
                                    {{--                                        <div class="catalog__item-wrap">--}}
                                    {{--                                            <div class="catalog__item-favorite">--}}
                                    {{--                                                <img--}}
                                    {{--                                                    src="img/main-page/catalog-favorite.svg"--}}
                                    {{--                                                    alt=""--}}
                                    {{--                                                />--}}
                                    {{--                                            </div>--}}
                                    {{--                                            <div class="catalog__item-new">new</div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <img--}}
                                    {{--                                            class="catalog__item-img"--}}
                                    {{--                                            src="img/main-page/catalogs-img.svg"--}}
                                    {{--                                            alt=""--}}
                                    {{--                                        />--}}
                                    {{--                                        <div class="catalog__item-title">--}}
                                    {{--                                            Сюжетно-ролевые игрушки Mary Poppins ....--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <div class="catalog__item-subtitle">--}}
                                    {{--                                            Набор для творчества Полесье, 15 элементов, цвет в--}}
                                    {{--                                            ассортименте--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <div class="catalog__item-wrap2">--}}
                                    {{--                                            <div class="catalog__item-stock">Есть на складе</div>--}}
                                    {{--                                            <div class="catalog__item-cost">4&nbsp; ₸</div>--}}
                                    {{--                                            <div class="catalog__item-basket">--}}
                                    {{--                                                <img src="img/main-page/catalog-basket.svg" alt=""/>--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <footer class="footer">
        <div class="container-fluid">
            <div class="footer__inner">
                <button onclick="topFunction()" id="myBtn" class="footer__up">
                    <img src="{{asset('img/main-page/footer-arrow.svg')}}" alt=""/>
                </button>
                <div class="footer__links offset-md-2 col-md-8">
                    <a href="{{ route('delivery') }}" class="footer__link">Доставка и оплата</a>
                    {{--                    <a href="" class="footer__link">Условия возврата</a>--}}
                    <a href="{{ route('company') }}" class="footer__link">О компании</a>
                    <a href="{{ route('contacts') }}" class="footer__link">Контакты</a>
                </div>
                <div class="footer__wrap">
                    <a href="" class="footer__social"
                    ><img src="{{asset('img/main-page/vk.svg')}}" alt=""
                        /></a>
                    <a href="" class="footer__social">
                        <img src="{{asset('img/main-page/insta.svg')}}" alt=""/> </a
                    ><a href="" class="footer__social"
                    ><img src="{{asset('img/main-page/facebook.svg')}}" alt=""
                        /></a>
                </div>
                <div class="footer__title">
                </div>
            </div>
        </div>
    </footer>
    <script>
        var mybutton = document.getElementById("myBtn");
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
@endsection
