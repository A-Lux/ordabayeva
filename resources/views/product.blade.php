@extends('layouts.header')
@section('content')
    <div class="container-fluid">
        <section class="product">
            <div class="product__inner">
                <div class="product__images">
                    @foreach(json_decode($products->image) as $image)
                        <img
                            class="catalog__item-img" style="width: 100%; height: 250px"
                            src="{{asset('storage/'.$image)}}"
                            alt=""
                        />
                        @break
                    @endforeach
                    <div class="product__images-wrap">
                        @foreach(json_decode($products->image) as $image)
                            <img
                                class="catalog__item-img"
                                {{--                                style="width: 100%; height: 250px"--}}
                                src="{{asset('storage/'.$image)}}"
                                alt=""
                            />
                        @endforeach
                    </div>
                </div>
                <div class="product__wrap">
                    <div class="product__title">{!! $products->product_name !!}</div>
                    <div
                        class="product__stock">{!! $products->stock == 1 ?'Товар есть в наличии':'Товара нет в наличии' !!}</div>
                    <div class="product__subtitle">Описание</div>
                    <div class="product__descr">
                        {!! $products->description !!}
                    </div>
                    <div class="product__btns">
                        <button class="button product__button">{!! $products->price !!} тг.</button>
                        <div class="product__pieces">1 шт.</div>
                        <div class="product__favorite">
                            <a href="{{route('add_wishlist',$products['id'])}}">
                                <img src="{{asset('img/product-page/fav.svg')}}" alt=""/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section class="interest interest-etc">
        <div class="container-fluid">
            <div class="interest__inner">
                <div class="title">Похожие товары</div>
                <div class="interest__sliders">
                    @foreach($products->similar as $similar)
                        <div class="interest__slider inerest__slider1">
                            <div class="catalog__iteзm">
                                <div class="catalog__item-wrap">
                                    <div class="catalog__item-favorite">
                                        <img src="{{asset('img/main-page/catalog-favorite.svg')}}" alt=""/>
                                    </div>
                                    <div class="catalog__item-new">new</div>
                                </div>
                                <a href="{{ route('product_page', $similar['id']) }}">

                                @foreach(json_decode($similar->image) as $image)
                                    <img
                                        class="catalog__item-img" style="width: 100%; height: 250px"
                                        src="{{asset('storage/'.$image)}}"
                                        alt=""
                                    />
                                @endforeach
                                </a>
                                <div class="catalog__item-title">
{{--                                    {{ $similar->product_name }}--}}
                                    {{$similar['product_name'],$similar['id']}}

                                </div>
                                <div class="catalog__item-subtitle">
                                    {{ $similar->description }}
                                </div>
                                <div class="catalog__item-wrap2">
                                    <div class="catalog__item-stock">Есть на складе</div>
                                    <div class="catalog__item-cost">{{ $similar->price }} ₸</div>
                                    <div class="catalog__item-basket">
                                        <a href="{{route('add_cart',$similar['id'])}}">

                                        <img src="{{asset('img/main-page/catalog-basket.svg')}}" alt=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{--            <div class="interest__slider">--}}
                    {{--              <div class="catalog__item">--}}
                    {{--                <div class="catalog__item-wrap">--}}
                    {{--                  <div class="catalog__item-favorite">--}}
                    {{--                    <img src="{{asset('img/main-page/catalog-favorite.svg')}}" alt="" />--}}
                    {{--                  </div>--}}
                    {{--                  <div class="catalog__item-new">new</div>--}}
                    {{--                </div>--}}
                    {{--                <img--}}
                    {{--                  class="catalog__item-img"--}}
                    {{--                  src="{{asset('img/main-page/catalogs-img.svg')}}"--}}
                    {{--                  alt=""--}}
                    {{--                />--}}
                    {{--                <div class="catalog__item-title">--}}
                    {{--                  Сюжетно-ролевые игрушки Mary Poppins ....--}}
                    {{--                </div>--}}
                    {{--                <div class="catalog__item-subtitle">--}}
                    {{--                  Набор для творчества Полесье, 15 элементов, цвет в--}}
                    {{--                  ассортименте--}}
                    {{--                </div>--}}
                    {{--                <div class="catalog__item-wrap2">--}}
                    {{--                  <div class="catalog__item-stock">Есть на складе</div>--}}
                    {{--                  <div class="catalog__item-cost">4&nbsp;500 ₸</div>--}}
                    {{--                  <div class="catalog__item-basket">--}}
                    {{--                    <img src="{{asset('img/main-page/catalog-basket.svg')}}" alt="" />--}}
                    {{--                  </div>--}}
                    {{--                </div>--}}
                    {{--              </div>--}}
                    {{--            </div>--}}
                </div>
            </div>
        </div>
    </section>
    <footer class="footer">
        <div class="container-fluid">
            <div class="footer__inner">
                <button onclick="topFunction()" id="myBtn" class="footer__up">
                    <img src="{{asset('img/main-page/footer-arrow.svg')}}" alt=""/>
                </button>
                <div class="footer__links offset-md-2 col-md-8">
                    <a href="{{ route('delivery') }}" class="footer__link">Доставка и оплата</a>
                    {{--                    <a href="" class="footer__link">Условия возврата</a>--}}
                    <a href="{{ route('company') }}" class="footer__link">О компании</a>
                    <a href="{{ route('contacts') }}" class="footer__link">Контакты</a>
                </div>
                <div class="footer__wrap">
                    <a href="" class="footer__social"
                    ><img src="{{asset('img/main-page/vk.svg')}}" alt=""
                        /></a>
                    <a href="" class="footer__social">
                        <img src="{{asset('img/main-page/insta.svg')}}" alt=""/> </a
                    ><a href="" class="footer__social"
                    ><img src="{{asset('img/main-page/facebook.svg')}}" alt=""
                        /></a>
                </div>
                <div class="footer__title">
                </div>
            </div>
        </div>
    </footer>
    <script>
        var mybutton = document.getElementById("myBtn");
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
@endsection
