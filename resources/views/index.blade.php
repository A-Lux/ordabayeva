@extends('layouts.header')
@section('content')
    <body>
    <section class="main">
        <div class="main__inner">
            <div class="main__sliders">
                <div class="main__slider">
                    <div class="main__slider-wrap">
                        <div class="main__slider-title">- Happy Child -</div>
                    </div>
                </div>
                <div class="main__slider">
                    <div class="main__slider-wrap">
                        <div class="main__slider-title">- Happy Child -</div>
                    </div>
                </div>
                <div class="main__slider">
                    <div class="main__slider-wrap">
                        <div class="main__slider-title">- Happy Child -</div>
                    </div>
                </div>
                <div class="main__slider">
                    <div class="main__slider-wrap">
                        <div class="main__slider-title">- Happy Child -</div>
                    </div>
                </div>
                <div class="main__slider">
                    <div class="main__slider-wrap">
                        <div class="main__slider-title">- Happy Child -</div>
                    </div>
                </div>
                <div class="main__slider">
                    <div class="main__slider-wrap">
                        <div class="main__slider-title">- Happy Child -</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="cards">
        <div class="container-fluid">
            <div class="cards__inner">
                <div class="cards__items">
                    <div class="cards__item">
                        <a href="/shop/3">
                        <img
                            class="cards__item-img"
                            src="img/main-page/cards-img3.png"
                            alt=""
                        />
                        </a>
                        <div class="cards__item-title">игрушки</div>
                    </div>
                    <div class="cards__item">
{{--                        <a href="{{ route('shop') }}">--}}
                        <a href="/shop/2">
{{--                            @dd($category)--}}
                        <img
                            class="cards__item-img"
                            src="img/main-page/cards-img2.png"
                            alt=""
                        />
                        </a>
                        <div class="cards__item-title">косметика</div>
                    </div>
                    <div class="cards__item">
                        <a href="shop/4">
                        <img
                            class="cards__item-img"
                            src="img/main-page/cards-img3.png"
                            alt=""
                        />
                        </a>
                        <div class="cards__item-title">парфюм</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="catalog">
        <div class="container-fluid">
            <div class="catalog__inner">
                <div class="catalog__wrap">
                    <ul class="catalog__tabs">
                        <li class="catalog__tab catalog__tab-active">лидер продаж</li>
                        <li class="catalog__tab">ПОПУЛЯРНЫЕ</li>
                        <li class="catalog__tab">Новинки</li>
                    </ul>
                    <a href="{{ route('filter') }}">
                        <div class="catalog__show">показать все</div>
                    </a>
                </div>
                <div class="catalog__items catalog__items-active">
                    @foreach($leaderProducts as $product)
                        <div class="catalog__item" style="display:flex; flex-direction: column;">
                            <div class="catalog__item-wrap">
                                <div class="catalog__item-favorite">
                                    @if(\Illuminate\Support\Facades\Auth::check())
                                        <a href="{{route('add_wishlist',$product['id'])}}">
                                            @endif
                                            <div
                                                @if(!\Illuminate\Support\Facades\Auth::check()) data-modal="login" @endif>
                                                <img src="{{asset('img/main-page/catalog-favorite.svg')}}" alt=""/>
                                            </div>
                                            @if(\Illuminate\Support\Facades\Auth::check())
                                        </a>
                                    @endif
                                </div>
                                <div class="catalog__item-new">new</div>
                            </div>
                            @foreach(json_decode($product->image) as $image)
                                <a href="{{ route('product_page', $product['id']) }}">
                                    <img
                                        class="catalog__item-img" style="width: 100%; height: 250px"
                                        src="{{asset('storage/'.$image)}}"
                                        alt=""
                                    />
                                </a>
                                @break
                            @endforeach
                            <div class="catalog__item-title">
                                {{$product['product_name']}}
                            </div>
<!--
                            <div class="catalog__item-subtitle" style="flex: 1 1 auto;">
                                {{$product['description']}}
                            </div>
-->
                            <div class="catalog__item-wrap2">
                                <div
                                    class="catalog__item-stock">{{$product['stock'] == 1 ? 'Есть в наличии': 'Нет в наличии'}}</div>
                                <div class="catalog__item-cost">{{$product['price']}} ₸</div>
                                <div class="catalog__item-basket">
                                    <a href="{{route('add_cart',$product['id'])}}">
                                        <img src="{{asset('img/main-page/catalog-basket.svg')}}" alt=""/>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="catalog__items">
                    @foreach($popularProducts as $product)
                        <div class="catalog__item" style="display:flex; flex-direction: column;">
                            <div class="catalog__item-wrap">
                                <div href="" class="catalog__item-favorite">
                                    @if(\Illuminate\Support\Facades\Auth::check())
                                        <a href="{{route('add_wishlist',$product['id'])}}">
                                            @endif
                                            <div
                                                @if(!\Illuminate\Support\Facades\Auth::check()) data-modal="login" @endif>
                                                <img src="{{asset('img/main-page/catalog-favorite.svg')}}" alt=""/>
                                            </div>
                                            @if(\Illuminate\Support\Facades\Auth::check())
                                        </a>
                                    @endif
                                </div>
                                <div class="catalog__item-new">new</div>
                            </div>
                            <a href="{{ route('product_page', $product['id']) }}">
                                @foreach(json_decode($product->image) as $image)
                                    <img
                                        class="catalog__item-img" style="width: 100%; height: 250px"
                                        src="{{asset('storage/'.$image)}}"
                                        alt=""
                                    />
                                    @break
                                @endforeach
                            </a>
                            <div class="catalog__item-title">
                                {{$product['product_name'],$product['id']}}
                            </div>
<!--
                            <div class="catalog__item-subtitle" style="flex: 1 1 auto">
                                {{$product['description']}}
                            </div>
-->
                            <div class="catalog__item-wrap2">
                                <div
                                    class="catalog__item-stock">{{$product['stock']== 1 ? 'Есть в наличии': 'Нет в наличии'}}</div>
                                <div class="catalog__item-cost">{{$product['price']}} ₸</div>
                                <div class="catalog__item-basket">
                                    <a href="{{route('add_cart',$product['id'])}}">
                                        <img src="{{asset('img/main-page/catalog-basket.svg')}}">
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="catalog__items">
                    @foreach($newProducts as $product)
                        <div class="catalog__item" style="display: flex; flex-direction: column">
                            <div class="catalog__item-wrap">
                                <div class="catalog__item-favorite">
                                    @if(\Illuminate\Support\Facades\Auth::check())
                                        <a href="{{route('add_wishlist',$product['id'])}}">
                                            @endif
                                            <div
                                                @if(!\Illuminate\Support\Facades\Auth::check()) data-modal="login" @endif>
                                                <img src="{{asset('img/main-page/catalog-favorite.svg')}}" alt=""/>
                                            </div>
                                            @if(\Illuminate\Support\Facades\Auth::check())
                                        </a>
                                    @endif
                                </div>
                                <div class="catalog__item-new">new</div>
                            </div>
                            {{--                            @dd($product->image)--}}
                            <a href="{{ route('product_page', $product['id']) }}">
                                        @if(isset($product['image']))
                                @foreach(json_decode($product->image) as $image)
                                    <img
                                        class="catalog__item-img" style="width: 100%; height: 250px"
{{--                                        src="{{asset('storage/'.$image)}}"--}}
                                            src="{{\TCG\Voyager\Facades\Voyager::image($image)}}"
                                        alt=""
                                    />
                                    @break
                                @endforeach
                                            @endif
                            </a>
                            <div class="catalog__item-title">
                                {{$product['product_name']}}
                            </div>
<!--
                            <div class="catalog__item-subtitle" style="flex: 1 1 auto;">
                                {{$product['description']}}
                            </div>
-->
                            <div class="catalog__item-wrap2">
                                <div
                                    class="catalog__item-stock">{{$product['stock'] == 1 ? 'Есть в наличии': 'Нет в наличии'}}</div>
                                <div class="catalog__item-cost">{{$product['price']}} ₸</div>
                                <div class="catalog__item-basket">
                                    <a href="{{route('add_cart',$product['id'])}}">
                                        <img src="{{asset('img/main-page/catalog-basket.svg')}}" alt=""/>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <section class="best-deal">
        <div class="container-fluid">
            <div class="best-deal__inner">
                <div class="title">ЛУЧШИЕ ПРЕДЛОЖЕНИЯ</div>
                <div class="best-deal__items">
                    <div class="best-deal__item">
                        <div class="best-deal__item-img">
                            <a href="/shop/3">
                            <img src=" {{asset('img/main-page/best-deal-img2.png')}} " alt=""/>
                            </a>
                        </div>
                        <div class="best-deal__item-title">Плюшевые игрушки плюшки</div>
                    </div>
                    <div class="best-deal__item">
                        <div class="best-deal__item-img">
                            <a href="/shop/4">
                            <img src=" {{asset('img/main-page/best-deal-img2.png')}} " alt=""/>
                            </a>
                        </div>
                        <div class="best-deal__item-title">2+1 парфюм</div>
                    </div>
                    <div class="best-deal__item">
                        <div class="best-deal__item-img">
                            <a href="{{route('filter')}}">
                            <img src=" {{asset('img/main-page/best-deal-img2.png')}} " alt=""/>
                            </a>
                        </div>
                        <div class="best-deal__item-title">получите в подарок</div>
                    </div>
                    <div class="best-deal__item">
                        <div class="best-deal__item-img">
                            <a href="{{route('filter')}}">
                            <img src=" {{asset('img/main-page/best-deal-img2.png')}} " alt=""/>
                            </a>
                        </div>
                        <div class="best-deal__item-title">2 помады по цене одной</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="interest">
        <div class="container-fluid">
            <div class="interest__inner">
                <div class="title">Вас может заинтересовать</div>
                <div class="interest__sliders">
                    @foreach($interProducts as $product)
                        <div class="interest__slider">
                            <div class="interest__column" style="flex: 0 1 25%;">
                                <div class="catalog__item"
                                     style="display: flex; flex-direction: column; ">
                                    <div class="catalog__item-wrap">
                                        <div class="catalog__item-favorite">
                                            @if(\Illuminate\Support\Facades\Auth::check())
                                                <a href="{{route('add_wishlist',$product['id'])}}">
                                                    @endif
                                                    <div
                                                        @if(!\Illuminate\Support\Facades\Auth::check()) data-modal="login" @endif>
                                                        <img src="{{asset('img/main-page/catalog-favorite.svg')}}" alt=""/>
                                                    </div>
                                                    @if(\Illuminate\Support\Facades\Auth::check())
                                                </a>
                                            @endif
                                        </div>
                                        <div class="catalog__item-new">new</div>
                                    </div>
                                    @foreach(json_decode($product->image) as $image)
                                        <img
                                            class="catalog__item-img" style="width: 100%; height: 250px"
                                            src="{{asset('storage/'.$image)}}"
                                            alt=""
                                        />
                                        @break
                                    @endforeach
                                    <div class="catalog__item-title">
                                        {{$product['product_name']}}
                                    </div>
<!--
                                    <div class="catalog__item-subtitle" style="flex: 1 1 auto;">
                                        {{$product['description']}}
                                    </div>
-->
                                    <div class="catalog__item-wrap2">
                                        <div
                                            class="catalog__item-stock">{{$product['stock'] == 1 ? 'Есть в наличии': 'Нет в наличии'}}</div>
                                        <div class="catalog__item-cost">{{$product['price']}} ₸</div>
                                        <div class="catalog__item-basket">
                                            <a href="{{route('add_cart',$product['id'])}}">
                                                <img src="{{asset('img/main-page/catalog-basket.svg')}}" alt=""/>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <footer class="footer">
        <div class="container-fluid">
            <div class="footer__inner">
                <button onclick="topFunction()" id="myBtn" class="footer__up">
                    <img src="{{asset('img/main-page/footer-arrow.svg')}}" alt=""/>
                </button>
                <div class="footer__links offset-md-2 col-md-8">
                    <a href="{{ route('delivery') }}" class="footer__link">Доставка и оплата</a>
{{--                    <a href="" class="footer__link">Условия возврата</a>--}}
                    <a href="{{ route('company') }}" class="footer__link">О компании</a>
                    <a href="{{ route('contacts') }}" class="footer__link">Контакты</a>
                </div>
                <div class="footer__wrap">
                    <a href="" class="footer__social"
                    ><img src="{{asset('img/main-page/vk.svg')}}" alt=""
                        /></a>
                    <a href="" class="footer__social">
                        <img src="{{asset('img/main-page/insta.svg')}}" alt=""/> </a
                    ><a href="" class="footer__social"
                    ><img src="{{asset('img/main-page/facebook.svg')}}" alt=""
                        /></a>
                </div>
                <div class="footer__title">
                </div>
            </div>
        </div>
    </footer>
    <script>
        var mybutton = document.getElementById("myBtn");
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
@endsection
