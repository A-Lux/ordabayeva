<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Ordabayeva</title>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('css/style.min.css')}}" />
</head>

<body>
    <header class="header">
        <nav class="header__nav">
            <div class="container-fluid">
                <div class="row">
                    <div class="header__nav-wrap col-xl-6 col-lg-6 col-md-6">
                        <a class="header__nav-link" href="{{ route('company') }}">О компании</a>
                        <a class="header__nav-link" href="{{ route('delivery') }}">Условия доставки</a>
                        <a class="header__nav-link" href="{{ route('contacts') }}">Контакты</a>
                    </div>
                    <div class="header__nav-wrap2 offset-xl-3 col-xl-3 col-lg-6 col-md-6">
                        <a class="header__nav-location" href="">
                            <img src="{{asset('img/main-page/location.svg')}}" alt="location" />
                            <span class="city-dropdown">
                                <span class="city-name active">Алматы</span>
                                {{-- <div class="city-menu">--}}
                                {{-- <a href="#" class="city-choose">Астана</a>--}}
                                {{-- <a href="#" class="city-choose">Алматы</a>--}}
                                {{-- <a href="#" class="city-choose">Актобе</a>--}}
                                {{-- <a href="#" class="city-choose">Атырау</a>--}}
                                {{-- </div>--}}
                            </span>

                        </a>
                        <a class="header__nav-phone" href="">8 777 292 20 20</a>
                    </div>

                </div>
            </div>
        </nav>
        <div class="header__menu">
            <div class="container-fluid">
                <div class="header__inner">
                    <button class="header__catalog">
                        <div class="burger">
                            <span class="burger-line"></span>
                        </div>
                        Каталог
                    </button>
                    <a class="header__logo" href="{{ route('main') }}">
                        <img src="{{asset('img/main-page/logo.svg')}}" alt="" />
                    </a>

                    <div class="header__search">
                        <form action="{{ route('search') }}">
                            <div class="search-input">
                                <input type="search" name="product_name" class="form-controll" autocomplete="off" />
                                <div class="autocom-box">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="header__btns">
                        @if(\Illuminate\Support\Facades\Auth::check())
                        <a href="{{url('cabinet/75')}}">
                            @endif
                            <div class="header__favorite" @if(!\Illuminate\Support\Facades\Auth::check()) data-modal="login" @endif>
                                <img src="{{asset('img/main-page/header-favorite.svg')}}" alt="" />
                            </div>
                            @if(\Illuminate\Support\Facades\Auth::check())
                        </a>
                        @endif
                        <div class="header__basket">
                            <a href="{{ route('view_cart') }}">
                                <img src="{{asset('img/main-page/header-basket.svg')}}" alt="" />
                            </a>
                        </div>
                        @if(\Illuminate\Support\Facades\Auth::check())
                        <a href="{{url('cabinet/74')}}">
                            @endif
                            <div class="header__account" @if(!\Illuminate\Support\Facades\Auth::check()) data-modal="login" @endif>
                                <img src="{{asset('img/main-page/header-account.svg')}}" alt="" />
                            </div>
                            @if(\Illuminate\Support\Facades\Auth::check())
                        </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="header__offer">
            <h1 class="header__offer-h1">Оптовый магазин</h1>
            <h2 class="header__offer-h2">Мин. оптовый заказ от 50 000</h2>
        </div>
    </header>
    <section class="h-catalog">
        <div class="container-fluid">
            <div class="h-catalog__inner">
                <div class="h-catalog__wrap h-catalog__wrap1">
                    <div>
                        @foreach(\App\ShopCategory::where('parent_id',null)->get() as $category)
                        <div class="h-catalog__btn" onclick="window.location.href='{{route('shop',$category->id)}}'">{{$category->title}}</div>
                        @endforeach
                    </div>
                    <img src="{{asset('img/h-catalog1.png')}}" alt="" />
                </div>
                <div class="h-catalog__wrap h-catalog__wrap2">
                    <a href="{{route('filter')}}">
                        <div class="h-catalog__link">Корейская косметика</div>
                        <div class="h-catalog__link">Натуральная косметика</div>
                        <div class="h-catalog__link">Дермокосметика</div>
                        <div class="h-catalog__link">Уход и гигиена</div>
                        <div class="h-catalog__link">Парфюмерия</div>
                        <div class="h-catalog__link">Краска для волос</div>
                        <div class="h-catalog__link">Подарочные наборы</div>
                        <div class="h-catalog__link">Аксессуары и инструменты</div>
                        <div class="h-catalog__link">Медицинские приборы</div>
                    </a>
                </div>
                <div class="h-catalog__wrap h-catalog__wrap3">
                    <a href="{{route('filter')}}">
                        <div class="h-catalog__link">Корейская косметика</div>
                        <div class="h-catalog__link">Натуральная косметика</div>
                        <div class="h-catalog__link">Дермокосметика</div>
                        <div class="h-catalog__link">Уход и гигиена</div>
                        <div class="h-catalog__link">Парфюмерия</div>
                        <div class="h-catalog__link">Краска для волос</div>
                        <div class="h-catalog__link">Подарочные наборы</div>
                        <div class="h-catalog__link">Аксессуары и инструменты</div>
                        <div class="h-catalog__link">Медицинские приборы</div>
                    </a>
                </div>
                <div class="h-catalog__wrap h-catalog__wrap4">
                    <img src="{{asset('img/h-catalog2.png')}}" alt="" />
                </div>
            </div>
        </div>
    </section>
</body>
<!-- ---------------- Модалка Вход -------------------- -->
<div class="overlay2">
    <div class="modalLogin" id="login">
        <div class="modal__close">&times;</div>
        <div class="modal__title">Вход</div>
        <form method="post" action="{{ route('login') }}" class="modal__form">
            {{csrf_field()}}
            <div class="account__personal-date-box">
                <div class="account__personal-date-titles">E-mail</div>
                <input name="email" type="text" class="account__personal-date-input @error('email') is-invalid @enderror" value="{{ old('email') }}" required autocomplete="email" autofocus />
            </div>
            <div class="account__personal-date-box">
                <div class="account__personal-date-titles">Пароль</div>
                <input name="password" type="password" class="account__personal-date-input" />
            </div>
            <div class="modal__btns">
                <button type="submit" class="modal__btn modal__btn1">Войти</button>
                <button class="modal__btn modal__btn2" data-modal="registration">
                    Регистрация
                </button>
            </div>
        </form>
    </div>
</div>

<!-- ----------------- Модалка Регистрация-------------- -->
<div class="overlay3">
    <div class="modalRegistration" id="registration">
        <div class="modal__close">&times;</div>
        <div class="modal__title">Регистрация</div>
        <form method="POST" action="{{ route('register') }}" class="modal__form">
            @csrf

            <div class="account__personal-date-box">
                <label for="email" class="account__personal-date-titles">E-Mail</label>
                <input id="email" type="email" class="account__personal-date-input @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div class="account__personal-date-box">
                <label for="phone" class="account__personal-date-titles">Телефон</label>
                <input id="phone" type="text" class="account__personal-date-input" name="phone">
            </div>

            <div class="account__personal-date-box">
                <label for="password" class="account__personal-date-titles">Пароль</label>
                <input id="password" type="password" class="account__personal-date-input @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div class="account__personal-date-box">
                <label for="password-confirm" class="account__personal-date-titles">Подтвердите пароль</label>
                <input id="password-confirm" type="password" class="account__personal-date-input" name="password_confirmation" required autocomplete="new-password">
            </div>
            <div class="modal__btns">
                <button class="modal__btn modal__btn2" type="submit">Регистрация</button>
                <button class="modal__btn modal__btn1" data-modal="login">Войти</button>
            </div>
        </form>

    </div>
</div>

<script src="{{asset('https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js')}}"></script>
<script src="{{asset('js/libs.min.js')}}"></script>
<script src="{{asset('js/libs.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>

</html>
@yield('content')
