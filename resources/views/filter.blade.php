@extends('layouts.header')
@section('content')
    <section class="filter">
        <div class="container-fluid">
                <div class="filter__title">
{{--                {{ $title }}--}}
                </div>
        </div>
        <div class="filter__inner">
            <div class="filter__options">
                <form action="{{ route('filter') }}" method="get">
                    <div class="filter__box1">
                        <div class="filter__box1-title">
                            <p>Упорядочить по...</p>
                            <img src="{{asset('img/Union.svg')}}" alt=""/>
                        </div>
                        <div class="filter__box1-item">
                            <div class="filter__box1-link">Скидке</div>
                            <div class="filter__box1-link">Цене (по убыванию)</div>
                            <div class="filter__box1-link">Цене (по возрастанию)</div>
                            <div class="filter__box1-link">Рейтингу</div>
                            <div class="filter__box1-link">Дате добавления в каталог</div>
                        </div>
                    </div>
                    <div class="filter__box2 open-1">
                        <div class="filter__box-title">
                            <p>Категория</p>
                            <img src="img/Union.svg" alt=""/>
                        </div>
                        <div class="filter__none none-1">
                            @foreach($categories as $category)
                            <label class="filter__container"
                            >{{ $category->title }}
                                <input type="checkbox" name="category[]" value="{{ $category->id }}"/>
{{--                                @if( $category->id == true )--}}
{{--                                    <input TYPE="checkbox" value="{{ $category->id }}" checked>--}}
{{--                                @endif--}}
                                <span class="checkmark"></span>
                            </label>
                            @endforeach

                        </div>
                    </div>
                    <div class="filter__box2 open-2">
                        <div class="filter__box-title">
                            <p>Город</p>
                            <img src="{{ asset('img/Union.svg') }}" alt=""/>
                        </div>
                        <div class="filter__none none-2">
                            @foreach($cities as $city)
                            <label class="filter__container"
                            >{{$city->city}}
                                <input type="checkbox" name="city[]" value="{{$city->id}}"/>
                                <span class="checkmark"></span>
                            </label>
                            @endforeach
                        </div>
                    </div>
                    <div class="filter__box2 open-3">
                        <div class="filter__box-title">
                            <p>Бренд</p>
                            <img src="{{asset('img/Union.svg')}}" alt=""/>
                        </div>
                        <div class="filter__none none-3">
                            @foreach($brands as $brand)
                            <label class="filter__container"
                            >{{$brand->name}}
                                <input type="checkbox" name="brands[]" value="{{$brand->id}}"/>
                                <span class="checkmark"></span>
                            </label>
                            @endforeach
                        </div>
                    </div>
                    <div class="filter__box2">
                        <div class="filter__box-title">
                            <p>Цена <span>(тг)</span></p>
                        </div>
                            <input
                                class="filter__form-input1"
                                id="ot"
                                type="text"
                                type="number"
                                maxlength="8"
                                name="price_to"
                            />
                            <label for="ot" class="filter__form-lab1">от</label>
                            <input
                                class="filter__form-input2"
                                id="do"
                                type="text"
                                type="number"
                                maxlength="8"
                                name="price_from"
                            />
                            <label for="do" class="filter__form-lab2">до</label>
                    </div>
                    <div class="filter__box2 open-4">
                        <div class="filter__box-title">
                            <p>Пол</p>
                            <img src="{{asset('img/Union.svg')}}" alt=""/>
                        </div>
                        <div class="filter__none none-4">
                            <label class="filter__container"
                            >Мужской
                                <input type="checkbox" name="is_male[]"/>
                                <span class="checkmark"></span>
                            </label>
                            <label class="filter__container"
                            >Женский
                                <input type="checkbox" name="is_male[]"/>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                    <button class="button filter-button" type="submit">
                        Найти
                    </button>
                </form>
            </div>

            <div class="filter__catalogs">

                @foreach($products as $product)
                    <div class="catalog__item">
                        <div class="catalog__item-wrap">
                            <div class="catalog__item-favorite">
                                @if(\Illuminate\Support\Facades\Auth::check())
                                    <a href="{{route('add_wishlist',$product['id'])}}">
                                        @endif
                                        <div
                                            @if(!\Illuminate\Support\Facades\Auth::check()) data-modal="login" @endif>
                                            <img src="{{asset('img/main-page/catalog-favorite.svg')}}" alt=""/>
                                        </div>
                                        @if(\Illuminate\Support\Facades\Auth::check())
                                    </a>
                                @endif
                            </div>
                            <div class="catalog__item-new">new</div>
                        </div>
{{--                        @php $first_image = json_decode($product->image)[0]@endphp--}}
                        <a href="{{ route('product_page', $product['id']) }}">
                        <img
                            class="catalog__item-img"
                            style="width: 100%;"
{{--                            src="{{asset('storage/'.$first_image)}}"--}}
                            alt=""
                        />
                        </a>
                        <div class="catalog__item-title">
                            {{$product->product_name}}
                        </div>
                        <div class="catalog__item-subtitle">
                            {{ $product->description }}
                        </div>
                        <div class="catalog__item-wrap2">
                            <div class="catalog__item-stock">{{$product['stock'] == 1 ? 'Есть в наличии': 'Нет в наличии'}}</div>
                            <div class="catalog__item-cost">{{$product->price}} ₸</div>
                            <div class="catalog__item-basket">
                                <a href="{{route('add_cart',$product['id'])}}">
                                    <img src="{{asset('img/main-page/catalog-basket.svg')}}" alt=""/>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
                {{$products->links()}}
            </div>
{{--            {{$products->links}}--}}
        </div>
    </section>
    <footer class="footer">
        <div class="container-fluid">
            <div class="footer__inner">
                <button onclick="topFunction()" id="myBtn" class="footer__up">
                    <img src="{{asset('img/main-page/footer-arrow.svg')}}" alt=""/>
                </button>
                <div class="footer__links offset-md-2 col-md-8">
                    <a href="{{ route('delivery') }}" class="footer__link">Доставка и оплата</a>
                    {{--                    <a href="" class="footer__link">Условия возврата</a>--}}
                    <a href="{{ route('company') }}" class="footer__link">О компании</a>
                    <a href="{{ route('contacts') }}" class="footer__link">Контакты</a>
                </div>
                <div class="footer__wrap">
                    <a href="" class="footer__social"
                    ><img src="{{asset('img/main-page/vk.svg')}}" alt=""
                        /></a>
                    <a href="" class="footer__social">
                        <img src="{{asset('img/main-page/insta.svg')}}" alt=""/> </a
                    ><a href="" class="footer__social"
                    ><img src="{{asset('img/main-page/facebook.svg')}}" alt=""
                        /></a>
                </div>
                <div class="footer__title">
                </div>
            </div>
        </div>
    </footer>
    <script>
        var mybutton = document.getElementById("myBtn");
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
@endsection
