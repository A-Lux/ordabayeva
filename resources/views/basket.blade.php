@extends('layouts.header')
@section('content')
    <div class="container-fluid">
        <section class="basket">
            <div class="basket__inner">
                <div class="basket__title">Корзина товаров</div>
                <div class="row">
                    <div class="basket__items col-xl-9 col-md-8">
                        @if(isset($cartNew))
                            @foreach($cartNew as $product)
                                <div class="basket__item">
                                    <div class="basket__item-inner">
                                        <input type="checkbox" checked/>
                                        @if(isset($product['image']))
                                        @foreach(json_decode($product->image) as $image)
                                            <a href="{{ route('product_page', $product['id']) }}">
                                                <img
                                                    class="catalog__item-img" style="width: 100%; height: 250px"
                                                    src="{{asset('storage/'.$image)}}"
                                                    alt=""
                                                />
                                            </a>
                                            @break
                                        @endforeach
                                            @endif
                                    </div>
                                    <div class="basket__item-wrap">
                                        <p>
                                            {{$product->description}}
                                        </p>
                                        <div class="basket__item-clicks">
                                            <div class="basket__item-click">
                                                <a href="{{ route('add_wishlist', $product->id) }}">В избранное</a>
                                            </div>
                                            <div class="basket__item-click">
                                                <a href="{{ route('delete_cart', $product->id) }}">Удалить
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="basket__item-cost">
                                        <span>{{ $product->price }}</span>тг.
                                        @if($product->discount_price)
                                        <div class="basket__item-costDiscount">{{$product->discount_price}} тг</div>
                                            @endif
                                    </div>

                                    <div class="basket__item-counter">
                                        <div class="buttons">
                                            <button class="minusButton">
                                                <a href="{{ route('remove_cart',$product->id) }}">
                                                    <img src="img/minus.svg" alt=""/>
                                                </a>
                                            </button>
                                            <span class="amount">{{ $product->quantity }}</span>
                                            <button class="plusButton">
                                                <a href="{{ route('add_cart', $product->id) }}">
                                                    <img src="img/plus.svg" alt=""/>
                                                </a>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="basket__order col-xl-3 col-md-4">
                        <a href="{{ route('order_page') }}">
                            <button class="button basket__btn">
                                Оформить заказ
                            </button>
                        </a>
                        <div class="basket__order-title">Ваша корзина</div>
                        <div class="basket__order-wrap">
                            <div>Товары <span>{{ $product->quantity }}</span></div>
                            <div><span>{{ $product->price * $product->quantity}} </span>тг.</div>
                        </div>
                        <div class="basket__order-allCost">
                            <div>Общая стоимость</div>
                            <div><span>{{ $product->price * $product->quantity}}</span>тг.</div>
                        </div>
                        <div class="basket__order-min50">
                            Ваш оптовый заказ должен составлять сумму не менее 50 000 тенге
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
{{--    <section class="interest interest-etc">--}}
{{--        <div class="container-fluid">--}}
{{--            <div class="interest__inner">--}}
{{--                <div class="title">Похожие товары</div>--}}
{{--                <div class="interest__sliders">--}}
{{--                    <div class="interest__slider inerest__slider1">--}}
{{--                        <div class="catalog__item">--}}
{{--                            <div class="catalog__item-wrap">--}}
{{--                                <div class="catalog__item-favorite">--}}
{{--                                    <img src="img/main-page/catalog-favorite.svg" alt=""/>--}}
{{--                                </div>--}}
{{--                                <div class="catalog__item-new">new</div>--}}
{{--                            </div>--}}
{{--                            <img--}}
{{--                                class="catalog__item-img"--}}
{{--                                src="img/main-page/catalogs-img.svg"--}}
{{--                                alt=""--}}
{{--                            />--}}
{{--                            <div class="catalog__item-title">--}}
{{--                                Сюжетно-ролевые игрушки Mary Poppins ....--}}
{{--                            </div>--}}
{{--                            <div class="catalog__item-subtitle">--}}
{{--                                Набор для творчества Полесье, 15 элементов, цвет в--}}
{{--                                ассортименте--}}
{{--                            </div>--}}
{{--                            <div class="catalog__item-wrap2">--}}
{{--                                <div class="catalog__item-stock">Есть на складе</div>--}}
{{--                                <div class="catalog__item-cost">4&nbsp;500 ₸</div>--}}
{{--                                <div class="catalog__item-basket">--}}
{{--                                    <img src="img/main-page/catalog-basket.svg" alt=""/>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="interest__slider">--}}
{{--                        <div class="catalog__item">--}}
{{--                            <div class="catalog__item-wrap">--}}
{{--                                <div class="catalog__item-favorite">--}}
{{--                                    <img src="img/main-page/catalog-favorite.svg" alt=""/>--}}
{{--                                </div>--}}
{{--                                <div class="catalog__item-new">new</div>--}}
{{--                            </div>--}}
{{--                            <img--}}
{{--                                class="catalog__item-img"--}}
{{--                                src="img/main-page/catalogs-img.svg"--}}
{{--                                alt=""--}}
{{--                            />--}}
{{--                            <div class="catalog__item-title">--}}
{{--                                Сюжетно-ролевые игрушки Mary Poppins ....--}}
{{--                            </div>--}}
{{--                            <div class="catalog__item-subtitle">--}}
{{--                                Набор для творчества Полесье, 15 элементов, цвет в--}}
{{--                                ассортименте--}}
{{--                            </div>--}}
{{--                            <div class="catalog__item-wrap2">--}}
{{--                                <div class="catalog__item-stock">Есть на складе</div>--}}
{{--                                <div class="catalog__item-cost">4&nbsp;500 ₸</div>--}}
{{--                                <div class="catalog__item-basket">--}}
{{--                                    <img src="img/main-page/catalog-basket.svg" alt=""/>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="interest__slider">--}}
{{--                        <div class="catalog__item">--}}
{{--                            <div class="catalog__item-wrap">--}}
{{--                                <div class="catalog__item-favorite">--}}
{{--                                    <img src="img/main-page/catalog-favorite.svg" alt=""/>--}}
{{--                                </div>--}}
{{--                                <div class="catalog__item-new">new</div>--}}
{{--                            </div>--}}
{{--                            <img--}}
{{--                                class="catalog__item-img"--}}
{{--                                src="img/main-page/catalogs-img.svg"--}}
{{--                                alt=""--}}
{{--                            />--}}
{{--                            <div class="catalog__item-title">--}}
{{--                                Сюжетно-ролевые игрушки Mary Poppins ....--}}
{{--                            </div>--}}
{{--                            <div class="catalog__item-subtitle">--}}
{{--                                Набор для творчества Полесье, 15 элементов, цвет в--}}
{{--                                ассортименте--}}
{{--                            </div>--}}
{{--                            <div class="catalog__item-wrap2">--}}
{{--                                <div class="catalog__item-stock">Есть на складе</div>--}}
{{--                                <div class="catalog__item-cost">4&nbsp;500 ₸</div>--}}
{{--                                <div class="catalog__item-basket">--}}
{{--                                    <img src="img/main-page/catalog-basket.svg" alt=""/>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="interest__slider">--}}
{{--                        <div class="catalog__item">--}}
{{--                            <div class="catalog__item-wrap">--}}
{{--                                <div class="catalog__item-favorite">--}}
{{--                                    <img src="img/main-page/catalog-favorite.svg" alt=""/>--}}
{{--                                </div>--}}
{{--                                <div class="catalog__item-new">new</div>--}}
{{--                            </div>--}}
{{--                            <img--}}
{{--                                class="catalog__item-img"--}}
{{--                                src="img/main-page/catalogs-img.svg"--}}
{{--                                alt=""--}}
{{--                            />--}}
{{--                            <div class="catalog__item-title">--}}
{{--                                Сюжетно-ролевые игрушки Mary Poppins ....--}}
{{--                            </div>--}}
{{--                            <div class="catalog__item-subtitle">--}}
{{--                                Набор для творчества Полесье, 15 элементов, цвет в--}}
{{--                                ассортименте--}}
{{--                            </div>--}}
{{--                            <div class="catalog__item-wrap2">--}}
{{--                                <div class="catalog__item-stock">Есть на складе</div>--}}
{{--                                <div class="catalog__item-cost">4&nbsp;500 ₸</div>--}}
{{--                                <div class="catalog__item-basket">--}}
{{--                                    <img src="img/main-page/catalog-basket.svg" alt=""/>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="interest__slider">--}}
{{--                        <div class="catalog__item">--}}
{{--                            <div class="catalog__item-wrap">--}}
{{--                                <div class="catalog__item-favorite">--}}
{{--                                    <img src="img/main-page/catalog-favorite.svg" alt=""/>--}}
{{--                                </div>--}}
{{--                                <div class="catalog__item-new">new</div>--}}
{{--                            </div>--}}
{{--                            <img--}}
{{--                                class="catalog__item-img"--}}
{{--                                src="img/main-page/catalogs-img.svg"--}}
{{--                                alt=""--}}
{{--                            />--}}
{{--                            <div class="catalog__item-title">--}}
{{--                                Сюжетно-ролевые игрушки Mary Poppins ....--}}
{{--                            </div>--}}
{{--                            <div class="catalog__item-subtitle">--}}
{{--                                Набор для творчества Полесье, 15 элементов, цвет в--}}
{{--                                ассортименте--}}
{{--                            </div>--}}
{{--                            <div class="catalog__item-wrap2">--}}
{{--                                <div class="catalog__item-stock">Есть на складе</div>--}}
{{--                                <div class="catalog__item-cost">4&nbsp;500 ₸</div>--}}
{{--                                <div class="catalog__item-basket">--}}
{{--                                    <img src="img/main-page/catalog-basket.svg" alt=""/>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <footer class="footer">
        <div class="container-fluid">
            <div class="footer__inner">
                <button onclick="topFunction()" id="myBtn" class="footer__up">
                    <img src="{{asset('img/main-page/footer-arrow.svg')}}" alt=""/>
                </button>
                <div class="footer__links offset-md-2 col-md-8">
                    <a href="{{ route('delivery') }}" class="footer__link">Доставка и оплата</a>
                    {{--                    <a href="" class="footer__link">Условия возврата</a>--}}
                    <a href="{{ route('company') }}" class="footer__link">О компании</a>
                    <a href="{{ route('contacts') }}" class="footer__link">Контакты</a>
                </div>
                <div class="footer__wrap">
                    <a href="" class="footer__social"
                    ><img src="{{asset('img/main-page/vk.svg')}}" alt=""
                        /></a>
                    <a href="" class="footer__social">
                        <img src="{{asset('img/main-page/insta.svg')}}" alt=""/> </a
                    ><a href="" class="footer__social"
                    ><img src="{{asset('img/main-page/facebook.svg')}}" alt=""
                        /></a>
                </div>
                <div class="footer__title">
                </div>
            </div>
        </div>
    </footer>
    <script>
        var mybutton = document.getElementById("myBtn");
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
@endsection
