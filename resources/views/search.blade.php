<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
@if(isset($products))
    @foreach($products as $product)
        <div style="text-align: center">
            {{$product->product_name}}
        </div>
        <div style="text-align: center">
            {{$product->price}}
        </div>
        <div style="text-align: center">
            {{$product->stock == 1? 'Есть в наличии':'Нет в наличии'}}
        </div>

        <div style="text-align: center">
        <a href="{{route('add_cart', $product['id'])}}">
            Добавить
        </a>
        </div>
    @endforeach
@endif
</body>
</html>
