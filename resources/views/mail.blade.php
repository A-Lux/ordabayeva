<h3>Заказ номер {{$order->order_number != null ? $order->order_number : '#'.$order->created_at->day.$order->created_at->month.'-'.$order->created_at->year.'-'.$order->id}}</h3>
<p>Цена : {{$order->unit_price}}</p>
<p>Количество : {{$order->unit_quantity}}</p>
<p>Адрес : {{$order->address}}</p>
<p></p>
<table width="100%" cellpadding="0" cellspacing="0" style="min-width:100%;">
    <thead>
    <tr>
        <th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Продукт</th>
        <th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Количество</th>
        <th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Цена</th>
        <th scope="col" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;line-height:30px">Литраж</th>

    </tr>
    </thead>
    <tbody>
    @foreach($order_details as $detail)
        <tr>
{{--            <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">{{$detail->product->product_name}}</td>--}}
            <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">{{$detail->total_quantity}}</td>
            <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">{{$detail->total_price}}</td>
{{--            <td valign="top" style="padding:5px; font-family: Arial,sans-serif; font-size: 16px; line-height:20px;">{{$detail->variation != null ? $detail->variation->name : '' }}</td>--}}

        </tr>
    @endforeach
    </tbody>
</table>
