@extends('layouts.header')
@section('content')
    <section class="link1">
      <div class="container-fluid">
        <div class="link1__inner">
          <div class="link1__wrap2">
            <div class="link1__title">Контакты</div>
            <div class="link1__maps">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2906.383848519991!2d76.88976901548452!3d43.24337647913742!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836914fc0c842d%3A0x17469850ff87d28c!2z0YPQu9C40YbQsCDQqNC10LLRh9C10L3QutC-IDE2NdCwLCDQkNC70LzQsNGC0YsgMDUwMDAw!5e0!3m2!1sru!2skz!4v1605952155838!5m2!1sru!2skz"
                frameborder="0"
                style="border: 0"
                allowfullscreen=""
                aria-hidden="false"
                tabindex="0"
              ></iframe>
            </div>
            <div class="link1__descr link1__descr2">
              <p>Алматы: 8 (727) 330 88 66</p>
              <p>Нур-Султан (Астана): 8 (7172) 79 50 00</p>
              <p>Караганда: 8 (7212) 50 40 50</p>
              <p>Весь Казахстан: 8 800 080 35 47 (звонок бесплатный)</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <footer class="footer">
        <div class="container-fluid">
            <div class="footer__inner">
                <button onclick="topFunction()" id="myBtn" class="footer__up">
                    <img src="{{asset('img/main-page/footer-arrow.svg')}}" alt=""/>
                </button>
                <div class="footer__links offset-md-2 col-md-8">
                    <a href="{{ route('delivery') }}" class="footer__link">Доставка и оплата</a>
                    {{--                    <a href="" class="footer__link">Условия возврата</a>--}}
                    <a href="{{ route('company') }}" class="footer__link">О компании</a>
                    <a href="{{ route('contacts') }}" class="footer__link">Контакты</a>
                </div>
                <div class="footer__wrap">
                    <a href="" class="footer__social"
                    ><img src="{{asset('img/main-page/vk.svg')}}" alt=""
                        /></a>
                    <a href="" class="footer__social">
                        <img src="{{asset('img/main-page/insta.svg')}}" alt=""/> </a
                    ><a href="" class="footer__social"
                    ><img src="{{asset('img/main-page/facebook.svg')}}" alt=""
                        /></a>
                </div>
                <div class="footer__title">
                </div>
            </div>
        </div>
    </footer>
    <script>
        var mybutton = document.getElementById("myBtn");
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
@endsection
