@extends('layouts.header')
@section('content')
<div class="container-fluid">
    <section class="ordering">
        <form action="{{ route('order') }}">
            <div class="ordering__title">Оформление заказа</div>
            <div class="ordering__wrap">
                <div class="ordering__contacts">
                    <div class="ordering__payment-title">Контактные данные</div>
                    <div class="ordering__form1">
                        <div class="account__personal-date-box">
                            <div class="account__personal-date-titles">Имя</div>
                            <input name="name" type="text" value="{{ auth()->check() ? auth()->user()->name : ''}}" class="account__personal-date-input"/>
                        </div>
                        <div class="account__personal-date-box">
                            <div class="account__personal-date-titles">Телефон</div>
                            <input name="phone" type="text" value="{{ auth()->check() ? auth()->user()->phone : ''}}" class="account__personal-date-input"/>
                        </div>
                        <div class="account__personal-date-box">
                            <div class="account__personal-date-titles">E-mail</div>
                            <input name="email" type="text" value="{{ auth()->check() ? auth()->user()->email : ''}}" class="account__personal-date-input"/>
                        </div>
                    </div>
                </div>
                <div class="ordering__delivery">
                    <div class="ordering__payment-title">Оформление заказа</div>
                    <div class="ordering__form2">
                        <div class="account__personal-date-box">
                            <div class="account__personal-date-titles">Адрес</div>
                            <input name="address"  type="text" class="account__personal-date-input"/>
                        </div>
                        <div class="account__personal-date-box">
                            <div class="account__personal-date-titles">Страна,город</div>
                            <input name="country_city" type="text" class="account__personal-date-input"/>
                        </div>
                        <div class="ordering__form-wrap">
                            <div class="account__personal-date-box">
                                <div class="account__personal-date-titles">Подъезд</div>
                                <input name="porch" type="text" class="account__personal-date-input"/>
                            </div>
                            <div class="account__personal-date-box">
                                <div class="account__personal-date-titles">Индекс</div>
                                <input name="mail_index" type="text" class="account__personal-date-input"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ordering__delivery-descr">
                ВНИМАНИЕ: бесплатная доставка осуществляется от 5 000 тенге. До 5000
                тенге, сумма составляет 490 тенге.
            </div>
            <div class="ordering__payment">
                <div class="ordering__payment-inner">
                    <div class="ordering__payment-wrap">
                        <div class="ordering__payment-title">Способ оплаты</div>
                        <div class="ordering__payment-checkbox">
                            <input id="payment_type" value="Наличные" type="radio" name="payment_type"/>
                            <label for="payment_type">Наличные</label>
                        </div>
                        <div class="ordering__payment-checkbox">
                            <input id="payment_type" value="Платежная карта" type="radio" name="payment_type"/>
                            <label for="payment_type">Платежная карта</label>
                        </div>
                    </div>
                    <div class="ordering__payment-wrap2">
                        <div class="ordering__payment-title">Итого</div>
                        <div class="ordering__payment-wrap3">
                            <div class="ordering__payment-link">
                                Сумма товара: <span>{{ $totalSum }} тг.</span>
                            </div>
                            <div class="ordering__payment-link">
                                Доставка: <span>400 тг.</span>
                            </div>
                            <div class="ordering__payment-link">
                                Итого к оплате: <span>{{ $totalSum + 400 }} тг.</span>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::user() ? \Illuminate\Support\Facades\Auth::user()->id:null}}">
                <button type="submit" class="button ordering__payment-btn">
                    Подтвердить заказ
                </button>
            </div>
        </form>
    </section>
</div>
{{--<section class="interest interest-etc">--}}
{{--    <div class="container-fluid">--}}
{{--        <div class="interest__inner">--}}
{{--            <div class="title">Похожие товары</div>--}}
{{--            <div class="interest__sliders">--}}
{{--                <div class="interest__slider inerest__slider1">--}}
{{--                    <div class="catalog__item">--}}
{{--                        <div class="catalog__item-wrap">--}}
{{--                            <div class="catalog__item-favorite">--}}
{{--                                <img src="img/main-page/catalog-favorite.svg" alt=""/>--}}
{{--                            </div>--}}
{{--                            <div class="catalog__item-new">new</div>--}}
{{--                        </div>--}}
{{--                        <img--}}
{{--                            class="catalog__item-img"--}}
{{--                            src="img/main-page/catalogs-img.svg"--}}
{{--                            alt=""--}}
{{--                        />--}}
{{--                        <div class="catalog__item-title">--}}
{{--                            Сюжетно-ролевые игрушки Mary Poppins ....--}}
{{--                        </div>--}}
{{--                        <div class="catalog__item-subtitle">--}}
{{--                            Набор для творчества Полесье, 15 элементов, цвет в--}}
{{--                            ассортименте--}}
{{--                        </div>--}}
{{--                        <div class="catalog__item-wrap2">--}}
{{--                            <div class="catalog__item-stock">Есть на складе</div>--}}
{{--                            <div class="catalog__item-cost">4&nbsp;500 ₸</div>--}}
{{--                            <div class="catalog__item-basket">--}}
{{--                                <img src="img/main-page/catalog-basket.svg" alt=""/>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                @if($product)--}}
{{--                @foreach($product->similar as $similar)--}}
{{--                    <div class="interest__slider inerest__slider1">--}}
{{--                        <div class="catalog__item">--}}
{{--                            <div class="catalog__item-wrap">--}}
{{--                                <div class="catalog__item-favorite">--}}
{{--                                    <img src="{{asset('img/main-page/catalog-favorite.svg')}}" alt=""/>--}}
{{--                                </div>--}}
{{--                                <div class="catalog__item-new">new</div>--}}
{{--                            </div>--}}
{{--                            <img--}}
{{--                                class="catalog__item-img"--}}
{{--                                src="{{asset('img/main-page/catalogs-img.svg')}}"--}}
{{--                                alt=""--}}
{{--                            />--}}
{{--                            <div class="catalog__item-title">--}}
{{--                                {{ $similar->product_name }}--}}
{{--                            </div>--}}
{{--                            <div class="catalog__item-subtitle">--}}
{{--                                {{ $similar->description }}--}}
{{--                            </div>--}}
{{--                            <div class="catalog__item-wrap2">--}}
{{--                                <div class="catalog__item-stock">Есть на складе</div>--}}
{{--                                <div class="catalog__item-cost">{{ $similar->price }} ₸</div>--}}
{{--                                <div class="catalog__item-basket">--}}
{{--                                    <img src="{{asset('img/main-page/catalog-basket.svg')}}" alt=""/>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endforeach--}}
{{--                    @endif--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
<footer class="footer">
    <div class="container-fluid">
        <div class="footer__inner">
            <button onclick="topFunction()" id="myBtn" class="footer__up">
                <img src="{{asset('img/main-page/footer-arrow.svg')}}" alt=""/>
            </button>
            <div class="footer__links offset-md-2 col-md-8">
                <a href="{{ route('delivery') }}" class="footer__link">Доставка и оплата</a>
                {{--                    <a href="" class="footer__link">Условия возврата</a>--}}
                <a href="{{ route('company') }}" class="footer__link">О компании</a>
                <a href="{{ route('contacts') }}" class="footer__link">Контакты</a>
            </div>
            <div class="footer__wrap">
                <a href="" class="footer__social"
                ><img src="{{asset('img/main-page/vk.svg')}}" alt=""
                    /></a>
                <a href="" class="footer__social">
                    <img src="{{asset('img/main-page/insta.svg')}}" alt=""/> </a
                ><a href="" class="footer__social"
                ><img src="{{asset('img/main-page/facebook.svg')}}" alt=""
                    /></a>
            </div>
            <div class="footer__title">
            </div>
        </div>
    </div>
</footer>
<script>
    var mybutton = document.getElementById("myBtn");
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
</script>

@endsection
