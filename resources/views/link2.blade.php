@extends('layouts.header')
@section('content')
    <section class="link1">
      <div class="container-fluid">
        <div class="link1__inner">
          <div class="link1__wrap">
            <div class="link1__title">Условия доставки</div>
            <div class="link1__descr">
              <p>
                Внимание! С 25 мая изменились условия при оформлении заказов с
                курьерской доставкой:
              </p>
              <p>
                - при оформлении заказа на сумму свыше 3 000 тг – доставка
                бесплатная;
              </p>
              <p>
                - при оформлении заказа на сумму менее 3 000 тг – стоимость
                доставки 490 тг.
              </p>
            </div>
            <div class="link1__descr">
              <p>Города и сроки курьерской доставки:</p>
              <p>Алматы, Нур-Султан (Астана), Караганда, Темиртау — 1-4 дня</p>
              <p>Актобе — 8-10 дней</p>
              <p>Актау, Атырау, Уральск — 10-12 дне</p>
              <p>
                Костанай, Кокшетау, Павлодар, Петропавловск, Семей,
                Усть-Каменогорск, Шымкент, Тараз — 5-7 дней
              </p>
              <p>Жезказган, Сатпаев — 2-5 дней</p>
            </div>
            <div class="link1__descr">
              <p>
                Для получения оплаченного заказа необходимо предъявить курьеру
                любой документ, удостоверяющий личность получателя.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <footer class="footer">
        <div class="container-fluid">
            <div class="footer__inner">
                <button onclick="topFunction()" id="myBtn" class="footer__up">
                    <img src="{{asset('img/main-page/footer-arrow.svg')}}" alt=""/>
                </button>
                <div class="footer__links offset-md-2 col-md-8">
                    <a href="{{ route('delivery') }}" class="footer__link">Доставка и оплата</a>
                    {{--                    <a href="" class="footer__link">Условия возврата</a>--}}
                    <a href="{{ route('company') }}" class="footer__link">О компании</a>
                    <a href="{{ route('contacts') }}" class="footer__link">Контакты</a>
                </div>
                <div class="footer__wrap">
                    <a href="" class="footer__social"
                    ><img src="{{asset('img/main-page/vk.svg')}}" alt=""
                        /></a>
                    <a href="" class="footer__social">
                        <img src="{{asset('img/main-page/insta.svg')}}" alt=""/> </a
                    ><a href="" class="footer__social"
                    ><img src="{{asset('img/main-page/facebook.svg')}}" alt=""
                        /></a>
                </div>
                <div class="footer__title">
                </div>
            </div>
        </div>
    </footer>
    <script>
        var mybutton = document.getElementById("myBtn");
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
@endsection
