<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cascad</title>
</head>
<body>
@if(isset($cartNew))
    @foreach($cartNew as $product)
        <div class="col-12">
            <div class="card w-100 align-items-center d-flex justify-content-between flex-row mt-5"
                 style="text-align: center">
                <h4>
                    product {{ $product->id }}
                </h4>
                <h5 class="ml-5">
                    price {{ $product->price }}
                </h5>
                <h5 class="ml-5">
                    {{ $product->description }}
                </h5>
                <h5 class="ml-5">
                    quantity {{ $product->quantity }}
                </h5>
                <h5 class="ml-5">
                    total {{ $product->quantity * $product->price }}
                </h5>
                <a href="{{ route('remove_cart',$product->id) }}" class="btn btn-danger">
                    Delete
                </a>
                <div>
                    <a href="{{ route('order_page',$product->id) }}" class="btn btn-danger">
                        Order
                    </a>
                </div>
            </div>
        </div>
    @endforeach
@endif
</body>
</html>
