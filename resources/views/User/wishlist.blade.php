<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
@if(isset($wishlist))
    @foreach($wishlist as $room)
        <div class="col-12">
            <div class="card w-100 align-items-center d-flex justify-content-between flex-row mt-5"
                 style="text-align: center">
                <h4>
                    {{$room->product->product_name}}
                </h4>
                <h5 class="ml-5">
                    {{$room->product->description}}
                </h5>
                <h5 class="ml-5">
{{--                    {{$room->product_id}}--}}
                    <img src="{{asset('storage/'.$room->product['image'])}}">
                </h5>
                <a href="{{route('remove_wishlist',$room->id)}}" class="btn btn-danger">
                    Delete
                </a>
            </div>
        </div>
    @endforeach
@endif
<div>
</div>
</body>
</html>
