<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Ordabayeva</title>
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap"
        rel="stylesheet"
    />
    <link rel="stylesheet" href="/css/style.min.css"/>
</head>
<body>
<header class="header">
    <nav class="header__nav">
        <div class="container-fluid">
            <div class="row">
                <div class="header__nav-wrap col-xl-6 col-lg-6 col-md-6">
                    <a class="header__nav-link" href="">О компании</a>
                    <a class="header__nav-link" href="">Условия доставки</a>
                    <a class="header__nav-link" href="">Контакты</a>
                </div>
                <div
                    class="header__nav-wrap2 offset-xl-3 col-xl-3 col-lg-6 col-md-6"
                >
                    <a class="header__nav-location" href=""
                    >Алматы <img src="img/main-page/location.svg" alt="location"
                        /></a>
                    <a class="header__nav-phone" href="">8 777 292 20 20</a>
                </div>
            </div>
        </div>
    </nav>
    <div class="header__menu">
        <div class="container-fluid">
            <div class="header__inner">
                <a class="header__logo" href="">
                    <img src="img/main-page/logo.svg" alt=""/>
                </a>
                <button class="header__catalog">
                    <img src="img/main-page/header-catalog.svg" alt=""/> Каталог
                </button>
                <div class="header__search">
                    <form action="{{route('search')}}">
                        <input type="search" name="product_name" class="form-controll"/>
                    </form>
                </div>
                <div class="header__btns">
                    <div class="header__favorite">
                        <a href="{{route('view_wishlist')}}">
                            <img src="img/main-page/header-favorite.svg" alt=""/>
                        </a>
                    </div>
                    <div class="header__basket">
                        <a href="{{route('view_cart')}}">
                            <img src="img/main-page/header-basket.svg" alt=""/>
                        </a>
                    </div>
                    <div class="header__account">
                        <a href="{{route('home')}}">
                            <img src="img/main-page/header-account.svg" alt=""/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<section class="main">
    <div class="main__inner">
        <div class="main__sliders">
            <div class="main__slider">
                <div class="main__slider-wrap">
                    <div class="main__slider-title">- Happy Child -</div>
                </div>
            </div>
            <div class="main__slider">
                <div class="main__slider-wrap">
                    <div class="main__slider-title">- Happy Child -</div>
                </div>
            </div>
            <div class="main__slider">
                <div class="main__slider-wrap">
                    <div class="main__slider-title">- Happy Child -</div>
                </div>
            </div>
            <div class="main__slider">
                <div class="main__slider-wrap">
                    <div class="main__slider-title">- Happy Child -</div>
                </div>
            </div>
            <div class="main__slider">
                <div class="main__slider-wrap">
                    <div class="main__slider-title">- Happy Child -</div>
                </div>
            </div>
            <div class="main__slider">
                <div class="main__slider-wrap">
                    <div class="main__slider-title">- Happy Child -</div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="cards">
    <div class="container-fluid">
        <div class="cards__inner">
            <div class="cards__items">
                <div class="cards__item">
                    <img
                        class="cards__item-img"
                        src="img/main-page/cards-img3.png"
                        alt=""
                    />
                    <div class="cards__item-title">игрушки</div>
                </div>
                <div class="cards__item">
                    <img
                        class="cards__item-img"
                        src="img/main-page/cards-img2.png"
                        alt=""
                    />
                    <div class="cards__item-title">косметика</div>
                </div>
                <div class="cards__item">
                    <img
                        class="cards__item-img"
                        src="img/main-page/cards-img3.png"
                        alt=""
                    />
                    <div class="cards__item-title">парфюм</div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="catalog">
    <div class="container-fluid">
        <div class="catalog__inner">
            <div class="catalog__wrap">
                <ul class="catalog__tabs">
                    <li class="catalog__tab catalog__tab-active">лидер продаж</li>
                    <li class="catalog__tab">ПОПУЛЯРНЫЕ</li>
                    <li class="catalog__tab">Новинки</li>
                </ul>
                <a href="{{ route('view') }}">
                    <div class="catalog__show">показать все</div>
                </a>
            </div>
            <div class="catalog__items catalog__items-active">

            </div>
            {{--            <div class="catalog__item">--}}
            {{--              <div class="catalog__item-wrap">--}}
            {{--                <div class="catalog__item-favorite">--}}
            {{--                  <img src="img/main-page/catalog-favorite.svg" alt="" />--}}
            {{--                </div>--}}
            {{--                <div class="catalog__item-new">new</div>--}}
            {{--              </div>--}}
            {{--              <img--}}
            {{--                class="catalog__item-img"--}}
            {{--                src="img/main-page/catalogs-img.svg"--}}
            {{--                alt=""--}}
            {{--              />--}}
            {{--              <div class="catalog__item-title">--}}
            {{--                Сюжетно-ролевые игрушки Mary Poppins ....--}}
            {{--              </div>--}}
            {{--              <div class="catalog__item-subtitle">--}}
            {{--                Набор для творчества Полесье, 15 элементов, цвет в ассортименте--}}
            {{--              </div>--}}
            {{--              <div class="catalog__item-wrap2">--}}
            {{--                <div class="catalog__item-stock">Есть на складе</div>--}}
            {{--                <div class="catalog__item-cost">4&nbsp;500 ₸</div>--}}
            {{--                <div class="catalog__item-basket">--}}
            {{--                  <img src="img/main-page/catalog-basket.svg" alt="" />--}}
            {{--                </div>--}}
            {{--              </div>--}}
            {{--            </div>--}}

        </div>
    </div>
</section>
<section class="best-deal">
    <div class="container-fluid">
        <div class="best-deal__inner">
            <div class="title">ЛУЧШИЕ ПРЕДЛОЖЕНИЯ</div>
            <div class="best-deal__items">
                <div class="best-deal__item">
                    <div class="best-deal__item-img">
                        <img src="img/main-page/best-deal-img4.svg" alt=""/>
                    </div>
                    <div class="best-deal__item-title">Плюшевые игрушки плюшки</div>
                </div>
                <div class="best-deal__item">
                    <div class="best-deal__item-img">
                        <img src="img/main-page/best-deal-img2.png" alt=""/>
                    </div>
                    <div class="best-deal__item-title">2+1 парфюм</div>
                </div>
                <div class="best-deal__item">
                    <div class="best-deal__item-img">
                        <img src="img/main-page/best-deal-img4.svg" alt=""/>
                    </div>
                    <div class="best-deal__item-title">получите в подарок</div>
                </div>
                <div class="best-deal__item">
                    <div class="best-deal__item-img">
                        <img src="img/main-page/best-deal-img2.png" alt=""/>
                    </div>
                    <div class="best-deal__item-title">2 помады по цене одной</div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="interest">
    <div class="container-fluid">
        <div class="interest__inner">
            <div class="title">Вас может заинтересовать</div>
            <div class="interest__sliders">

                {{--                <div class="interest__slider">--}}
                {{--                    <div class="catalog__item">--}}
                {{--                        <div class="catalog__item-wrap">--}}
                {{--                            <div class="catalog__item-favorite">--}}
                {{--                                <img src="img/main-page/catalog-favorite.svg" alt=""/>--}}
                {{--                            </div>--}}
                {{--                            <div class="catalog__item-new">new</div>--}}
                {{--                        </div>--}}
                {{--                        <img--}}
                {{--                            class="catalog__item-img"--}}
                {{--                            src="img/main-page/catalogs-img.svg"--}}
                {{--                            alt=""--}}
                {{--                        />--}}
                {{--                        <div class="catalog__item-title">--}}
                {{--                            Сюжетно-ролевые игрушки Mary Poppins ....--}}
                {{--                        </div>--}}
                {{--                        <div class="catalog__item-subtitle">--}}
                {{--                            Набор для творчества Полесье, 15 элементов, цвет в--}}
                {{--                            ассортименте--}}
                {{--                        </div>--}}
                {{--                        <div class="catalog__item-wrap2">--}}
                {{--                            <div class="catalog__item-stock">Есть на складе</div>--}}
                {{--                            <div class="catalog__item-cost">4&nbsp;500 ₸</div>--}}
                {{--                            <div class="catalog__item-basket">--}}
                {{--                                <img src="img/main-page/catalog-basket.svg" alt=""/>--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>
        </div>
    </div>
</section>
<footer class="footer">
    <div class="container-fluid">
        <div class="footer__inner">
            <a href="index.html" class="footer__up">
                <img src="img/main-page/footer-arrow.svg" alt=""/>
            </a>
            <div class="footer__links offset-md-2 col-md-8">
                <a href="" class="footer__link">Доставка и оплата</a>
                <a href="" class="footer__link">Условия возврата</a>
                <a href="" class="footer__link">О компании</a>
                <a href="" class="footer__link">Контакты</a>
            </div>
            <div class="footer__wrap">
                <a href="" class="footer__social"
                ><img src="img/main-page/vk.svg" alt=""
                    /></a>
                <a href="" class="footer__social">
                    <img src="img/main-page/insta.svg" alt=""/> </a
                ><a href="" class="footer__social"
                ><img src="img/main-page/facebook.svg" alt=""/></a>
            </div>
            <div class="footer__title">
            </div>
        </div>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/js/libs.min.js"></script>
<script src="/js/main.js"></script>
</body>
</html>
