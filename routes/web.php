<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ProductController@ViewProducts')->name('main');
Route::get('/cabinet/{id}','WishlistController@cabinet')->name('cabinet');
Route::get('/company','Controller@company')->name('company');
Route::get('/delivery','Controller@delivery')->name('delivery');
Route::get('/contacts','Controller@contacts')->name('contacts');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/update','HomeController@updateUser')->name('update');

//Cart Routes
Route::get('cart/add/{product}','CartController@AddCart')->name('add_cart');
Route::get('/cart','CartController@ViewCart')->name('view_cart');
Route::get('/cart/remove/{product}','CartController@RemoveCart')->name('remove_cart');
Route::get('/cart/delete/{product}', 'CartController@deleteCart')->name('delete_cart');
//Route::get('/cart/remove/{id}','CartController@RemoveCart')->name('remove_cart');

//Wishlist Routes
Route::middleware(['auth'])->group(function (){
    Route::get('/wishlist','WishlistController@ViewWishlist')->name('view_wishlist');
    Route::get('/wishlist/add/{id}','WishlistController@AddWishlist')->name('add_wishlist');
    Route::get('/wishlist/remove/{id}','WishlistController@RemoveWishlist')->name('remove_wishlist');
});

//Product Routes
Route::get('/categories/{category}', 'ProductController@CategoryCont');
Route::get('/search','ProductController@SearchProducts')->name('search');
Route::get('/product', 'ProductController@view')->name('view')->middleware('auth');
Route::get('/product/about/{id}','ProductController@about')->name('product_page');
Route::get('/categories','ProductController@filter')->name('filter');
Route::get('/shop/{categoryId?}','ProductController@index')->name('shop');
Route::get('/test', 'ProductController@test')->name('test');
//Route::get('/index/shop/{categoryId?}','ProductController@index1')->name('shop1');

Route::get('/json', function (Request $request){
//   $json = file_get_contents(storage_path('json/sait.json'));
//   $objs = json_decode($json,true);
//
////   $duplicate = \App\Product::where('product_code',$request->product_code);
////
////   if ($duplicate){
////       echo "Product Code is Already in Use";
////   }
//
//       foreach ($objs as $obj) {
//           foreach ($obj as $key => $value) {
//               $insertArr[Str::slug($key, '_')] = $value;
//           }
////           \Illuminate\Support\Facades\DB::table('products')->insert($insertArr);
////           \App\Product::updateOrCreate($insertArr);
//           \App\Product::updateOrCreate(['product_code'=> $insertArr['product_code']],
//               [
//                    'product_name' =>$insertArr['product_name'],
//                    'price' => $insertArr['price'],
//                    'stock' => $insertArr['stock'],
//                    'description' => $insertArr['description'],
//                    'city' => $insertArr['city'],
//
//               ]);
//
//       };
//       dd("Done");

});

//Order Routes
Route::get('order', 'OrderController@order')->name('order');
Route::get('ordering', 'OrderController@orderPage')->name('order_page');
