<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopCategory extends Model
{
    use SoftDeletes;
    public function attributes()
    {
        return $this->hasMany('App\CategoryAttribute','shop_category_id','id');
    }
    public function parents()
    {
        return $this->belongsTo('App\ShopCategory','id','parent_id');
    }
    public function children()
    {
        return $this->hasMany('App\ShopCategory','parent_id','id');
    }
}
