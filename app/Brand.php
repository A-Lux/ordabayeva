<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    public function brand()
    {
        return $this->hasMany('App\Product','id','brand_id');
    }
}
