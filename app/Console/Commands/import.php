<?php

namespace App\Console\Commands;

use App\Product;
use http\Env\Request;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class import extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = '1c:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $json = file_get_contents(storage_path('json/sait.json'));
        $objs = json_decode($json,true);

        foreach ($objs as $obj) {
            foreach ($obj as $key => $value) {
                $insertArr[Str::slug($key, '_')] = $value;
            }
            Product::updateOrCreate(['product_code'=> $insertArr['product_code']],
                [
                    'product_name' =>$insertArr['product_name'],
                    'price' => $insertArr['price'],
                    'stock' => $insertArr['stock'],
                    'description' => $insertArr['description'],
                    'city' => $insertArr['city'],
                ]);

        };
        return 0;
    }
}
