<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $fillable=[
        'name',
        'surname',
        'phone',
        'address',
        'payment_type',
        'order',
        'payment_status_id',
        'order_status_id',
        'delivery_id',
        'delivery_status_id',
        'user_id',
        'country_city',
        'mail_index',
        'delivery_time',
        'total_price',
        'total_quantity',
        'delivery_price',
        'order_number',
        'porch'
    ];

    public function order_details()
    {
        return $this->hasMany('App\OrderDetail');
    }

    public function order_statuses()
    {
        return $this->hasMany('App\OrderStatus');
    }

}
