<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function company()
    {
        return view('link1');
    }

    public function delivery()
    {
        return view('link2');
    }

    public function contacts()
    {
        return view('link3');
    }
    public function navbar(){
        return view('layouts.header');
    }
}
