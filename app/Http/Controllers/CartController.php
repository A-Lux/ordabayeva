<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function ViewCart(Product $product)
    {
        $cart = session()->get('cart');
        if (!$cart) {
            return view('basket',compact('cart','product'))->with('message', 'gay club');
        } else {
            $cartNew = [];
            foreach ($cart as $productId => $quantity) {
                $cartProduct = Product::find($productId);
                $cartProduct->quantity = $quantity;
                $cartNew[] = $cartProduct;
            }
        }
//        session()->put('cart',[]);
//        session()->save();
        return view('basket', compact('cartNew'));
    }

    public function AddCart(Request $request, Product $product)
    {
        $cart = session()->get('cart');
        if(isset($cart[$product->id])){
            $cart[$product->id] = $cart[$product->id] + 1;
        }
        else {
            $cart[$product->id] = 1;
        }
        session()->forget('cart');
        session()->put('cart', $cart);

//        $cart = session()->get('cart');
////        session()->put('cart', []);
//        if (!$cart) {
//            $cart = [];
//            $newProduct['quantity'] = 1;
//            $newProduct['product_id'] = $product->id;
//            $cart[] = [
//                $product->id => 1
//            ];
//            session()->put('cart', $cart);
//            session()->save();
//        } else {
//            $exception = 0;
//            foreach ($cart as $key => $newProduct) {
//                if (isset($newProduct['product_id'])) {
//                    if ($newProduct['product_id'] == $product->id) {
//                        $exception = 1;
//                        $newProduct['quantity'] += 1;
//                        unset($cart[$key]);
//                        $cart[] = $newProduct;
//                        break;
//                    }
//                }
//            }
//            if ($exception == 0) {
//                $newProduct['product_id'] = $product->id;
//                $newProduct['quantity'] = 1;
//                $cart[] = $newProduct;
//            }
//            session()->put('cart',array_values($cart));
//            session()->save();
//        }
////        dd($cart);
//        $cart = session()->get('cart');

        return redirect()->route('view_cart')->with('message', 'Продукт добавлен в корзину!');
    }
    public function RemoveCart(Product $product)
    {
        $cart = session()->get('cart');
        if(isset($cart[$product->id])){
            if($cart[$product->id] > 1){
                $cart[$product->id] = $cart[$product->id] - 1;
            }
            else{
                unset($cart[$product->id]);
            }
        }
        session()->forget('cart');
        session()->put('cart', $cart);
//        $cart = session()->get('cart');
//        $product = Product::find($id);
//        foreach ($cart as $productID => $remProduct) {
//            if (isset($remProduct['product_id'])) {
//                if ($remProduct['product_id'] == $id){
//                    $cart[$key]['quantity'] -= 1;
//                    if ($cart[$key]['quantity'] <= 0){
//                        unset($cart[$key]);
//                    }
//                    break;
//                }
//            }
//        }
//        session()->put('cart', $cart);
//        session()->save();
        return redirect()->back();
    }

    public function deleteCart(Product $product)
    {
        $cart = session()->get('cart');
        unset($cart[$product->id]);
        session()->forget('cart');
        session()->put('cart', $cart);
        return redirect()->back();
    }
}
