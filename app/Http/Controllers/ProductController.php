<?php

namespace App\Http\Controllers;

use App\Brand;
use App\City;
use App\Product;
use App\ShopCategory;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Category;
use TCG\Voyager\Voyager;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        if ($id != null){
            $category = ShopCategory::find($id);
        }else{
            $category = null;
        }
        $search = null;
        if ($request->has('search')){
            $search = $request->search;
        }
        $brand = null;
        if ($request->has('brand')){
            $brand = $request->brand;
        }
        $products = Product::with('category')->where('shop_category_id', $id)->paginate('12');
//        dd($products);


        $categories = ShopCategory::all();
//        $categories = ShopCategory::with();
//        $products = Product::with('category');
        $cities = City::get();
        $brands = Brand::get();
//        $about = Page::where('page', 'about-me')->first();
       $title = ShopCategory::select('title')->first();
//        $title = ShopCategory::where('title')->first();
//       dd($title);
//        $title = $categories['title'];
//        dd($categories);
//        $title = 'Каталог';
//        dd($products);
        return view('filter',compact(
            'category',
            'categories',
            'search',
            'brand',
            'title',
            'cities',
            'brands',
            'products'
        ));
    }


    public function ViewProducts()
    {
        $leaderProducts = Product::where('is_leader', 1)->get();
        $popularProducts = Product::where('is_popular', 1)->get();
        $interProducts = Product::where('is_interesting', 1)->get();
        $newProducts = Product::orderBy('id', 'desc')->get();
        $cities = City::all();
//        $category = Category::get();

        return view('index', compact('newProducts', 'popularProducts', 'leaderProducts', 'interProducts','cities'));
    }

    public function view()
    {
        return view('account');
    }

    public function SearchProducts(Request $request)
    {
        $cities = City::get();
        $brands = Brand::get();
        $categories = ShopCategory::get();
        $search = $request->input('product_name');
        $products = Product::where('product_name', 'like', '%' . $search . '%')
            ->with('category')
            ->paginate(9);
        $title = $request->input('product_name');
        return view('filter', compact(
            'products',
            'cities',
            'brands',
            'categories',
            'title'
        ));
    }

    public function about($id)
    {
        $products = Product::with('similar')->find($id);

        if($products){
            return view('product', compact('products'));
        }else{
            return redirect()->back();
        }
    }

    public function filter(Request $request,$id=null)
    {
        $cities = City::get();
        $brands = Brand::get();
        $categories = ShopCategory::get();
        $title = 'Каталог';
        $products = Product::where(function($query) use ($request,$id){
            if( $id != null ){

            }
            if ($request->has('city')){
                $query->whereIn('city_id',$request->city);
            }

            if ($request->has('category')){
                $query->whereIn('shop_category_id',$request->category);
            }

            if ($request->has('brands')){
                $query->whereIn('brand_id',$request->brands);
            }

            if ($request->has('price_from') && $request->price_from !=''){
                $query->where('price','<=',$request->price_from);
            }

            if ($request->has('price_to') && $request->price_to != ''){
                $query->where('price','>=',$request->price_to);
            }

            if ($request->has('sort_type') && $request->has('sort_value')){
                $query->orderBy($request->sort_type,$request->sort_value);
            }

        })->paginate(12);
        $requests = $request->getContent();
        return view('filter',compact(
            'products','requests','cities','brands','categories', 'title'));
    }
        public function test(){
        $items = json_decode(file_get_contents('C:\Users\Admin.TAIRURALOV\Desktop\Folder\Lessons\projects\ordabayeva\resources\json\sait (1).json'));
        dd($items);
        }
}
