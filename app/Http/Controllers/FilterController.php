<?php

namespace App\Http\Controllers;

use App\Product;
use App\ShopCategory;
use Illuminate\Http\Request;

class FilterController extends Controller
{
    public function filter(Request $request)
    {
//        $categories = ShopCategory::where('parent_id',$request->)->get();
        $products = Product::with('variations')->where(function ($query) use ($request){
            if ($request->has('price')){
                $query->whereBetween('price',$request->price);
            }
            if ($request->has('category')){
                $query->whereBetween('category',$request->category);
            }
            if ($request->has('search')){
                $query->where('name','LIKE', '%'.$request->search.'%');
            }
        })->orderBy('id','desc')->paginate(10);
        return response(['products'=>$products],200);
    }

    public function category(Request $request)
    {
        if($request->has('category')){
            $category = ShopCategory::where('parent_id',$request->category)->get();
            return response(['categories'=>$category],200);
        }
    }
}
