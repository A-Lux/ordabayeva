<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderDetail;
use App\Product;
use App\ProductVariation;
use App\ShopCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use TCG\Voyager\Models\Category;

class OrderController extends Controller
{
//    public function order1()
//    {
//        $basket = session()->get('basket');
//        $basketMain = [];
//        $sum = 0;
//        $quantitySum = 0;
//        foreach ($basket as $id => $quantity) {
//            $product = Product::find($quantity['product_id']);
//            $product['quantity'] = $quantity['quantity'];
//            $basketMain = $product;
//            $quantitySum += $product['quantity'];
//
//            if ($product['category'] != null) {
//                $sum += $product['category']['price'] * $product['quantity'];
//            } else {
//                $sum += $product->price * $quantity['quantity'];
//            }
//            $quantitySum += $product['quantity'];
//        }
//        return view('', compact('sum', 'quantitySum'));
//    }

    public function viewOrder()
    {
        $order = Order::with('order_details')->where('user_id', Auth::user()->id)->orderBy('created_at','desc')->get();
        if(is_null($order)){
            return response('В Заказах отсутствуют продукты', 404);
        }
        return view('account', compact('order'));
    }

    public function order(Request $request, Product $product)
    {
        $valData = $request->validate([
            'name' => 'required',
            'porch' => 'required',
            'phone' => 'required',
            'address' => 'required',
//            'country_city' => 'required',
            'email'=>'required',
//            'mail_index'=>'required',
//            'order_number'=>'required'
            'payment_type'=>'required'
        ]);

        $basket = session()->get('cart');
        $totalQuantity = 0;
        $totalSum = 0;


        foreach ($basket as $id => $quantity) {
            $product = Product::with('similar')->find($id);
            $totalQuantity += $quantity;

            $totalSum += $quantity * $product->price;
        }
//        dd($product);

//        if($totalSum < 50000){
//            return redirect()->back()->withErrors('Заказ оформляется с 50000');
//        }

        $order = Order::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'user_id' => $request->has('user_id') ? $request->user_id : null,
            'email' => $request->has('email') ? $request->email : null,
//            'surname'=> $request->surname,
            'payment_type' => $request->payment_type,
            'payment_status_id' => 1,
//            'delivery_id' => 1,
            'delivery_status_id' => 1,
            'order_status_id' => 1,
            'address' => $request->address,
            'country_city' => $request->country_city,
            'mail_index' => $request->has('mail_index') ? $request->mail_index : null,
            'total_price' => $totalSum, //вот эту хуйню ебучую закинуть повыше прописать и сделать с ретёрном
            //минимальная стоимость заказа и доставки должна быть регулируемой в админке,
            'total_quantity' => $totalQuantity,
            'delivery_price' => $totalSum >= 60000 ? 0 : 4000,
            'porch'=>$request->porch
        ]);


        $order->update([
            'order_number' => '#' . $order->created_at->day . $order->created_at->month . '-' . $order->created_at->year . '-' . $order->id
        ]);
        $orderDetails = collect([]);
        foreach ($basket as $id => $quantity) {
            $product = Product::find($id);
            $orderDetails->push(OrderDetail::create([
                'unit_price'=>$product->price,
                'unit_quantity' => $quantity,
                'order_id' => $order->id,
                'product_name' => $product->product_name,
                'product_id'=>$product->id,
                'image'=>$product->image
            ]));
        }


//        $orderDetails = OrderDetail::with(['product', 'variation'])->where('order_id', $order->id)->get();
        session()->put('cart', []);
        session()->save();
//        dd($product);
//        Mail::send('mail', ['order' => $order, 'order_details' => $orderDetails], function ($message) {
//            $message->to('admin@admin.com', 'Toy Store')->subject('Заказ');
//            $message->from('info_toystore@gmail.com','toy_store.kz');
//        });
//
//        if($order->email != null){
//            Mail::send('mail',['order'=>$order,'order_details'=>$orderDetails], function ($message) use ($order){
//                $message->to($order->email, 'Toy Store')->subject('Заказ');
//                $message->from('info_toystore@gmail.com','Toy Store');
//            });
////            return response([
////                'message' => 'Заказ номер '.$order->order_number.' успешно создано',
////                //'redirect_url' => route('pay',$order->id)
////            ], 200);
//        }
        return redirect('/');
    }

    public function orderPage(Request $request)
    {
//        $order = Order::find($id);
        $cart = session()->get('cart');

        $totalSum = 0;
        foreach ($cart as $productId => $quantity){
            $product = Product::with('similar')->find($productId);
            $totalSum += $quantity * $product->price;
        }
        return view('ordering', compact('totalSum'));
    }
}
