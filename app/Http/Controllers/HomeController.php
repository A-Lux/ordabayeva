<?php

namespace App\Http\Controllers;

//use http\Client\Curl\User;
use Illuminate\Http\Request;
use TCG\Voyager\Models\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//        return view('user.home');
        return redirect()->back();
    }

    public function cabinet($id)
    {
        $user = Auth::user();
        dd($user);
        return view('account',compact($user));
    }


    public function updateUser(Request $request)
    {
        $userDetails = Auth::user();
        $user = User::find($userDetails->id);
//        dd($user);
  //      $test = $user->name;
//        dd($test);
//        $user = Auth::user();
        if ($request->has('name') && $request->name != '') {
            $user->name = $request->name;
        }
        if ($request->has('phone') && $request->phone != '') {
            $user->phone = $request->phone;
        }
        if ($request->has('is_male') && $request->is_male != '') {
//            $user['is_male'] = 1;
            $user->is_male = 1;
//            $user->avatar = setting('site.man');
        }
        if ($request->has('is_female')){
//            $user['is_male'] = 0;
            $user->is_male = 0;
//            $user->avatar = setting('site.female');
        }

        if ($request->has('email') && $request->email != ''){
            $user->email = $request->email;
        }
        if ($request->has('surname') && $request->email != ''){
            $user->surname = $request->surname;
        }
        session()->put('user',$user);
        session()->save();
        $user->save();
        return back();
    }

}
