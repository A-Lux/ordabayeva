<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\User;
use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishlistController extends Controller
{
//    public function AddWishlist($id){
//        $wishlist = new Wishlist();
//        $product = Product::find($id);
////        dd($wishlist);
//        if(!$product){
//            return redirect()->with('message','Данного продукта нет в наличии');
//        }
//        if(!$wishlist){
//            $wishlist = [];
//            $wish_item['product_id'] = $id;
//            $wishlist = $wish_item;
//            session()->put('wishlist',$wishlist);
//            session()->save();
//        }else{
//            foreach ($wishlist as $key=>$wish_item){
//                if(isset($wish_item['product_id'])){
//                    if($wish_item['product_id']==$id){
//                        return response('Данный товар уже есть в Избранных',404);
//                        break;
//                    }
//                }
//            }
//            $wishlist->save();
//            session()->put('wishlist',array_values($wishlist));
//            session()->save();
//        }
//        $wishlist = session('wishlist',$wishlist);
//    }
//
//    public function ViewWishlist(){
//        $wishlist = Wishlist::paginate(5);
//        return view('wishlist',$wishlist);
//    }

    public function AddWishlist($id,Request $request){
        $wishlist = Wishlist::where('user_id',$request->user_id)->where('product_id',$id)->first();
        $product = Product::find($id);
        if (!$product){
            return redirect()->back()->with('message','Данного продукта нет в наличии');
        }
        if(!$wishlist){
//            dd($wishlist);
            Wishlist::create([
                'user_id' => Auth::user()->id,
                'product_id' => $id
            ]);
        }
        return redirect()->back()->with('message','Продукт добавлен в Избранные!');
    }


    public function cabinet(){
        $wishlist = Wishlist::with('product')->where('user_id',Auth::user()->id)->get();
        $order = Order::with('order_details')->where('user_id', Auth::user()->id)->orderBy('created_at','desc')->get();
        if(is_null($wishlist)){
            return response('В Избранных отсутствуют продукты', 404);
        }else{
//            dd($wishlist);
        }
        $user = Auth::user();
//        dd($wishlist);
        return view('account',compact('wishlist', 'order','user'));
    }

    public function RemoveWishlist($id, Request $request){
        $wishlist = Wishlist::with('product')->where('user_id',Auth::user()->id)->find($id);
//        $product = Product::find($id);
        if ($wishlist){
//            dd($wishlist);
            $wishlist->delete();
        }
        return redirect()->back();
    }
}
