<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{

    protected $fillable = [
        'product_code', 'product_name', 'price', 'stock', 'description', 'discount_price', 'brand', 'city'
    ];

    use SoftDeletes;
    public function category()
    {
        return $this->belongsTo('App\ShopCategory','shop_category_id','id');
    }
    public function brand()
    {
        return $this->belongsTo('App\Brand', 'brand_id','id');
    }
    public function city()
    {
        return $this->belongsTo('App\City', 'city_id','id');
    }

    public function similar()
    {
        return $this->belongsToMany(\App\Product::class, 'similar_products', 'product_id', 'similar_id');
    }
}
