<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use SoftDeletes;
    protected $fillable=[
        'order_id','product_name','unit_quantity','unit_price','product_id','image'
    ];
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
    public function variation()
    {
        return $this->belongsTo('App\ProductVariation');
    }
}
