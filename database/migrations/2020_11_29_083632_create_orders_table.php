<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('payment_id')->constrained();
            $table->foreignId('payment_status_id')->constrained();
            $table->foreignId('delivery_id')->constrained();
            $table->foreignId('delivery_status_id')->constrained();
            $table->foreignId('user_id')->nullable()->constrained();
            $table->string('name');
            $table->string('phone');
            $table->string('email')->nullable();
            $table->string('country_city')->nullable();
            $table->string('address')->nullable();
            $table->string('mail_index')->nullable();
            $table->dateTime('delivery_time')->nullable();
            $table->integer('total_price');
            $table->integer('total_quantity');
            $table->integer('delivery_price');
//            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
